import { randomBytes, createHash } from 'crypto';
import base64url from 'base64url';
import * as bcrypt from 'bcrypt';

export class Helpers {
	/**
	 * Retorna tipo da variavel
	 * @param value
	 */
	static typeOf(value: any): string {
		let type = Object.prototype.toString.call(value);
		if (type === '[object Boolean]') return 'boolean';
		if (type === '[object Object]') return 'object';
		if (type === '[object Array]') return 'array';
		if (type === '[object String]') return 'string';
		return type;
	}

	/**
	 * Mescola com recursividade, sobreescreve objA
	 * @param value
	 */
	static mergeDeepRight(objA: any, objB: any): any {
		/**
		 * Se for object define na key correspondente
		 */
		if (Helpers.typeOf(objA) == 'object') {
			let newVar = {};

			/**
			 * Verifica todos os itens da objeto B primeiro
			 * definindo preferencia no objeto B
			 */
			for (const key in objB) {
				if (objB.hasOwnProperty(key)) {
					const elA = objA[key];
					const elB = objB[key];

					/**
					 * Se não forem do mesmo tipo, dá preferencia para o objB
					 * Se não existe valor no objA deixa como objB
					 */
					if (elA === undefined || Helpers.typeOf(elA) !== Helpers.typeOf(elB)) {
						newVar[key] = elB;
					} else {
						/**
						 * Se for objeto ou array
						 * faz o processo novamente
						 */
						if (Helpers.typeOf(elB) == 'object' || Helpers.typeOf(elB) == 'array') {
							newVar[key] = Helpers.mergeDeepRight(elA, elB);
						} else {
							newVar[key] = elB;
						}
					}
				}
			}

			/**
			 * Resgata os valores que existe no objeto A e
			 * que não existem no B
			 */
			for (const key in objA) {
				if (objA.hasOwnProperty(key)) {
					const elA = objA[key];
					const elB = objB[key];

					if (elB === undefined) {
						newVar[key] = elA;
					}
				}
			}

			return newVar;

			/**
			 * Se for array adicionar com push
			 */
		} else if (Helpers.typeOf(objA) == 'array') {
			let newVar = [];

			for (const key in objB) {
				if (objB.hasOwnProperty(key)) {
					const elA = objA[key];
					const elB = objB[key];

					/**
					 * Se não forem do mesmo tipo, dá preferencia para o objA
					 */
					if (elA === undefined || Helpers.typeOf(elA) !== Helpers.typeOf(elB)) {
						newVar.push(elB);
					} else {
						if (Helpers.typeOf(elB) == 'object' || Helpers.typeOf(elB) == 'array') {
							newVar.push(Helpers.mergeDeepRight(elA, elB));
						} else {
							newVar.push(elB);
						}
					}
				}
			}

			for (const key in objA) {
				if (objA.hasOwnProperty(key)) {
					const elA = objA[key];
					const elB = objB[key];

					if (elB === undefined) {
						newVar.push(elA);
					}
				}
			}

			return newVar;
		}
	}

	/**
	 * Verificar se string é email
	 * @param email email
	 */
	static verifyEmail(email: string): boolean {
		let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}

	/**
	 * Verifica se é um CPF valido
	 * @param cpf
	 */
	static verifyCPF(cpf: string): boolean {
		if (cpf === undefined || !cpf) {
			return false;
		}

		cpf = cpf.toString().replace(/[^0-9]/g, '');

		if (cpf.length != 11) {
			return false;
		}

		if (/(\d)\1{10}/.test(cpf)) {
			return false;
		}

		var Soma = 0;
		var Resto;

		for (let i = 1; i <= 9; i++) Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (11 - i);
		Resto = (Soma * 10) % 11;

		if (Resto == 10 || Resto == 11) Resto = 0;
		if (Resto != parseInt(cpf.substring(9, 10))) return false;

		Soma = 0;
		for (let i = 1; i <= 10; i++) Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (12 - i);
		Resto = (Soma * 10) % 11;

		if (Resto == 10 || Resto == 11) Resto = 0;
		if (Resto != parseInt(cpf.substring(10, 11))) return false;
		return true;
	}

	/**
	 * Converter valor em sha1
	 * @param value
	 */
	static sha1(value): string {
		return createHash('sha1')
			.update(value)
			.digest('hex');
	}

	/**
	 * Converter valor em md5
	 * @param value
	 */
	static md5(value): string {
		return createHash('md5')
			.update(value)
			.digest('hex');
	}

	static generateUserToken() {
		return base64url(randomBytes(20)) + '-' + Date.now().toString(16);
	}

	static getHashPassword(password: string, saltRounds = 13) {
		return bcrypt.hash(password, saltRounds);
	}

	static compareHashPassword(password: string, hash: string) {
		return bcrypt.compare(password, hash);
	}
}
