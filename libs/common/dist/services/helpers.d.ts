export declare class Helpers {
    static typeOf(value: any): string;
    static mergeDeepRight(objA: any, objB: any): any;
    static verifyEmail(email: string): boolean;
    static verifyCPF(cpf: string): boolean;
    static sha1(value: any): string;
    static md5(value: any): string;
    static generateUserToken(): string;
    static getHashPassword(password: string, saltRounds?: number): Promise<string>;
    static compareHashPassword(password: string, hash: string): Promise<boolean>;
}
