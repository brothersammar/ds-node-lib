"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = require("crypto");
const base64url_1 = require("base64url");
const bcrypt = require("bcrypt");
class Helpers {
    static typeOf(value) {
        let type = Object.prototype.toString.call(value);
        if (type === '[object Boolean]')
            return 'boolean';
        if (type === '[object Object]')
            return 'object';
        if (type === '[object Array]')
            return 'array';
        if (type === '[object String]')
            return 'string';
        return type;
    }
    static mergeDeepRight(objA, objB) {
        if (Helpers.typeOf(objA) == 'object') {
            let newVar = {};
            for (const key in objB) {
                if (objB.hasOwnProperty(key)) {
                    const elA = objA[key];
                    const elB = objB[key];
                    if (elA === undefined || Helpers.typeOf(elA) !== Helpers.typeOf(elB)) {
                        newVar[key] = elB;
                    }
                    else {
                        if (Helpers.typeOf(elB) == 'object' || Helpers.typeOf(elB) == 'array') {
                            newVar[key] = Helpers.mergeDeepRight(elA, elB);
                        }
                        else {
                            newVar[key] = elB;
                        }
                    }
                }
            }
            for (const key in objA) {
                if (objA.hasOwnProperty(key)) {
                    const elA = objA[key];
                    const elB = objB[key];
                    if (elB === undefined) {
                        newVar[key] = elA;
                    }
                }
            }
            return newVar;
        }
        else if (Helpers.typeOf(objA) == 'array') {
            let newVar = [];
            for (const key in objB) {
                if (objB.hasOwnProperty(key)) {
                    const elA = objA[key];
                    const elB = objB[key];
                    if (elA === undefined || Helpers.typeOf(elA) !== Helpers.typeOf(elB)) {
                        newVar.push(elB);
                    }
                    else {
                        if (Helpers.typeOf(elB) == 'object' || Helpers.typeOf(elB) == 'array') {
                            newVar.push(Helpers.mergeDeepRight(elA, elB));
                        }
                        else {
                            newVar.push(elB);
                        }
                    }
                }
            }
            for (const key in objA) {
                if (objA.hasOwnProperty(key)) {
                    const elA = objA[key];
                    const elB = objB[key];
                    if (elB === undefined) {
                        newVar.push(elA);
                    }
                }
            }
            return newVar;
        }
    }
    static verifyEmail(email) {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    static verifyCPF(cpf) {
        if (cpf === undefined || !cpf) {
            return false;
        }
        cpf = cpf.toString().replace(/[^0-9]/g, '');
        if (cpf.length != 11) {
            return false;
        }
        if (/(\d)\1{10}/.test(cpf)) {
            return false;
        }
        var Soma = 0;
        var Resto;
        for (let i = 1; i <= 9; i++)
            Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;
        if (Resto == 10 || Resto == 11)
            Resto = 0;
        if (Resto != parseInt(cpf.substring(9, 10)))
            return false;
        Soma = 0;
        for (let i = 1; i <= 10; i++)
            Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;
        if (Resto == 10 || Resto == 11)
            Resto = 0;
        if (Resto != parseInt(cpf.substring(10, 11)))
            return false;
        return true;
    }
    static sha1(value) {
        return crypto_1.createHash('sha1')
            .update(value)
            .digest('hex');
    }
    static md5(value) {
        return crypto_1.createHash('md5')
            .update(value)
            .digest('hex');
    }
    static generateUserToken() {
        return base64url_1.default(crypto_1.randomBytes(20)) + '-' + Date.now().toString(16);
    }
    static getHashPassword(password, saltRounds = 13) {
        return bcrypt.hash(password, saltRounds);
    }
    static compareHashPassword(password, hash) {
        return bcrypt.compare(password, hash);
    }
}
exports.Helpers = Helpers;
//# sourceMappingURL=helpers.js.map