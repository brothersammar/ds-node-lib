---
id: typeorm-crud
title: TypeOrm CRUD
sidebar_label: TypeOrm CRUD
---

TypeORM CRUD facilita o uso do CRUD no backend, exemplo de uso:

```ts
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TypeOrmCrudManagerService } from 'devesharp-node/libs/nestjs';
import { UsersStub } from './users-stub.entity';

@Injectable()
export class UsersService extends TypeOrmCrudManagerService<UsersStub> {
    constructor(@InjectRepository(UsersStub) private readonly userRepository: Repository<UsersStub>) {
        super(userRepository);
    }
}
```


