---
id: auth.module
title: Auth Module
sidebar_label: Auth Module
---

Modulo de autentificação para Mongoose e typeORM

## Mongoose

Para usar a autentificação com o mongoose

```ts
import { AuthModule } as 'devesharp-node/libs/nestjs';

@Module({
    imports: [
        AuthModule.forMongoose({
            secretKey: string;
            usersTable?: string; // default: users
            usersLoginColumn?: string[]; // default: ['login', 'email']
            usersPasswordColumn?: string; // default: 'password'
            userTokenTable?: string; // default: 'users_tokens'
        })
    ]
})
```

## TypeORM

Para usar a autentificação com o typeORM

```ts
import { AuthModule } as 'devesharp-node/libs/nestjs';

@Module({
    imports: [
        AuthModule.forTypeORM({
            secretKey: string;
            usersTable?: string; // default: users
            usersLoginColumn?: string[]; // default: ['login', 'email']
            usersPasswordColumn?: string; // default: 'password'
            userTokenTable?: string; // default: 'users_tokens'
        }),
    ]
})
```

## Guard

Para autentificar as rotas apenas importar AuthGuard, retona usuário em `res.user`

```ts
import { AuthGuard } as 'devesharp-node/libs/nestjs';

@Controller('foo')
@UseGuards(AuthGuard)
export class FooController {
    constructor() {}

    @Post()
    createLog(@Request() res: any) {
        console.log(res.user);
        return this.logRoutesService.create(body);
    }
}

```

# Rotas

Ao importar os modulos serão adicionas as seguintes rotas a aplicação:

### `/auth`

Allow the Authenticated User to update their details.

**URL** : `/auth`

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

**Data constraints**

```json
{
    "login": "[1 to 100 chars]",
    "password": "[1 to 100 chars]"
}
```