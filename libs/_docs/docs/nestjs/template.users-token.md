---
id: template.users-token
title: UsersTokens (Model)
sidebar_label: UsersTokens (Model)
---

Modelo de UsersToken para mongoose e typeorm:

## Mysql

```ts
import { UsersTokenEntity } as 'devesharp-node/libs/nestjs';

@Module({
    imports: [
        DatabaseModule.mysql({
            mysqlEntities: [ UsersTokenEntity ];
        }),
    ]
})
```