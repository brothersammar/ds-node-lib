---
id: database.module
title: Database Module
sidebar_label: Database Module
---

Modulo para conexão de banco dados MongoDB e Mysql

## MongoDB

Conexão com mongodb

```ts
import { DatabaseModule } as 'devesharp-node/libs/nestjs';

@Module({
    imports: [
        DatabaseModule.mongodb({
            mongodbHost?: string;
            mongodbDatabase: string;
            mongodbPort?: number;
            mongodbUser?: string;
            mongodbPassword?: string;
            mongodbAuthSourge?: string;
        }),
    ]
})
```

## Mysql

Conexão com mysql

```ts
import { DatabaseModule } as 'devesharp-node/libs/nestjs';

@Module({
    imports: [
        DatabaseModule.mysql({
            mysqlType?: 'mysql' | 'mariadb';
            mysqlHost?: string;
            mysqlPort?: number;
            mysqlUsername?: string;
            mysqlPassword?: string;
            mysqlDatabase?: string;
            mysqlEntities?: (Function | string | EntitySchema<any>)[];
            mysqlSynchronize?: boolean;
        }),
    ]
})
```