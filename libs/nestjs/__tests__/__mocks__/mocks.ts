import { TypeOrmModule } from '@nestjs/typeorm';
import { MongooseModule } from '@nestjs/mongoose';

export class Mocks {
    static databaseTypeOrm(entities: any = [`${__dirname  }/**/*.entity{.ts,.js}`]) {
        return [
            TypeOrmModule.forRoot({
                type: 'mysql',
                host: 'localhost',
                port: 9311,
                username: 'root',
                password: '123456aa',
                database: 'testing',
                entities,
                keepConnectionAlive: true,
                // logging: true,
                synchronize: true,
            }),
        ];
    }

    static databaseMongoose() {
        return [
            MongooseModule.forRoot('mongodb://admin:123456aa@localhost:9312/testing?authSource=admin', {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            }),
        ];
    }
}
