export interface IBodySearch<T> {
    query?: {
        limit?: number;
        offset?: number;
        sort?: number;
    };
    filters?: T;
    alias?: string[];
}
export interface IResultSearch<T> {
    count: number;
    results: T[];
}
export interface IArgsMethod<B> {
    model?: B;
    data?: any;
}
export interface IFilter {
    column: string;
    filter: 'int' | 'intGt' | 'intLt' | 'intGte' | 'intLte' | 'string' | 'arrayInt' | 'arrayIntNot' | 'arrayString' | 'arrayStringNot' | 'stringLike' | 'stringLikeNot' | 'stringLikePercent' | 'stringLikePercentAfter' | 'stringLikePercentBefore' | 'intInColumn';
}
export interface ISortSet {
    alias: string;
    name: string;
}
export interface IQuery {
    limit?: number | string;
    offset?: number | string;
    page?: number | string;
    sorts?: string[];
}
