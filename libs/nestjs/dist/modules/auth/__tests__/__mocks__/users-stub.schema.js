"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.UsersStubSchema = new mongoose.Schema({
    login: { type: String },
    email: { type: String },
    password: { type: String },
});
//# sourceMappingURL=users-stub.schema.js.map