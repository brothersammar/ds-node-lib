"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.UsersTokensStubSchema = new mongoose.Schema({
    userId: { type: String },
    token: { type: String, unique: true },
});
//# sourceMappingURL=users-tokens-stub.schema.js.map