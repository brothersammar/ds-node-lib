import { BaseEntity } from 'typeorm';
export declare class UsersStubEntity extends BaseEntity {
    id: number;
    login: string;
    email: string;
    password: string;
}
