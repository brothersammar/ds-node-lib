import * as mongoose from 'mongoose';
export interface IUsersStub extends mongoose.Document {
    login: string;
    email: string;
    password: string;
}
export declare const UsersStubSchema: mongoose.Schema;
