import { BaseEntity } from 'typeorm';
export declare class UsersTokenStubEntity extends BaseEntity {
    id: number;
    userId: number;
    token: string;
}
