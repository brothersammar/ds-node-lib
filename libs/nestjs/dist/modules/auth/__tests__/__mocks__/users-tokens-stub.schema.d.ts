import * as mongoose from 'mongoose';
export interface IUsersTokensStub extends mongoose.Document {
    userId: string;
    token: string;
}
export declare const UsersTokensStubSchema: mongoose.Schema;
