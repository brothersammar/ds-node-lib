export interface IAuthConfig {
    secretKey: string;
    usersTable?: string;
    usersLoginColumn?: string[];
    usersPasswordColumn?: string;
    userTokenTable?: string;
}
