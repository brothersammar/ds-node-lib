"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var AuthModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const auth_typeorm_service_1 = require("./auth-typeorm.service");
const auth_controller_1 = require("./auth.controller");
const config_service_1 = require("./config.service");
const auth_mongoose_service_1 = require("./auth-mongoose.service");
let AuthModule = AuthModule_1 = class AuthModule {
    static forMongoose(options) {
        return {
            module: AuthModule_1,
            imports: [
                jwt_1.JwtModule.register({
                    secret: 'SECRET_KEY',
                    signOptions: { expiresIn: '360d' },
                }),
            ],
            providers: [
                {
                    provide: 'AuthService',
                    useClass: auth_mongoose_service_1.AuthMongooseService,
                },
                {
                    provide: 'CONFIG_OPTIONS',
                    useValue: options,
                },
                config_service_1.AuthConfigService,
            ],
            exports: [],
            controllers: [auth_controller_1.AuthController],
        };
    }
    static forTypeORM(options) {
        return {
            module: AuthModule_1,
            imports: [
                jwt_1.JwtModule.register({
                    secret: 'SECRET_KEY',
                    signOptions: { expiresIn: '360d' },
                }),
            ],
            providers: [
                {
                    provide: 'AuthService',
                    useClass: auth_typeorm_service_1.AuthTypeOrmService,
                },
                {
                    provide: 'CONFIG_OPTIONS',
                    useValue: options,
                },
                config_service_1.AuthConfigService,
            ],
            exports: [],
            controllers: [auth_controller_1.AuthController],
        };
    }
};
AuthModule = AuthModule_1 = __decorate([
    common_1.Global(),
    common_1.Module({})
], AuthModule);
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map