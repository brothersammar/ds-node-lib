import { AuthDto } from './dto/auth-dto';
import { AuthService } from './auth.abstract.service';
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    auth(body: AuthDto): any;
}
