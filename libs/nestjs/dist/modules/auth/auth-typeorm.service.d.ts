import { Connection } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { AuthDto } from './dto/auth-dto';
import { AuthConfigService } from './config.service';
import { AuthService } from './auth.abstract.service';
export declare class AuthTypeOrmService extends AuthService {
    private connection;
    private readonly authConfigService;
    private readonly jwtService;
    constructor(connection: Connection, authConfigService: AuthConfigService, jwtService: JwtService);
    validateUser(jwt: string): Promise<any>;
    login(data: AuthDto): Promise<any>;
    generateToken(id: number | string): Promise<string>;
}
