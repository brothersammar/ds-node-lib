import { DynamicModule } from '@nestjs/common';
import { IAuthConfig } from './interfaces';
export declare class AuthModule {
    static forMongoose(options: IAuthConfig): DynamicModule;
    static forTypeORM(options: IAuthConfig): DynamicModule;
}
