import { AuthDto } from './dto/auth-dto';
export declare abstract class AuthService {
    abstract validateUser(token: string): Promise<any>;
    abstract login(data: AuthDto): any;
}
