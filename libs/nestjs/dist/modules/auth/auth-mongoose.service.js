"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const jwt_1 = require("@nestjs/jwt");
const common_2 = require("@devesharp/common");
const config_service_1 = require("./config.service");
const auth_abstract_service_1 = require("./auth.abstract.service");
let AuthMongooseService = class AuthMongooseService extends auth_abstract_service_1.AuthService {
    constructor(connection, authConfigService, jwtService) {
        super();
        this.connection = connection;
        this.authConfigService = authConfigService;
        this.jwtService = jwtService;
    }
    async validateUser(jwt) {
        const { userTokenTable } = this.authConfigService.get();
        const { usersTable } = this.authConfigService.get();
        const payload = this.jwtService.decode(jwt);
        if (!payload)
            return false;
        const userToken = await this.connection.collection(userTokenTable).findOne({
            token: payload.token,
        });
        if (!userToken)
            throw new common_1.UnauthorizedException();
        const user = await this.connection.collection(usersTable).findOne({
            id: userToken.userId,
        });
        if (!user)
            throw new common_1.UnauthorizedException();
        return user;
    }
    async login(data) {
        const { usersTable } = this.authConfigService.get();
        const { usersLoginColumn } = this.authConfigService.get();
        const queryBuilder = this.connection.collection('users');
        const query = [];
        usersLoginColumn.forEach(element => {
            query.push({
                [element]: data.login,
            });
        });
        const user = await queryBuilder.findOne({
            $or: query,
        });
        if (!user) {
            throw new common_1.UnauthorizedException('Dados incorretos');
        }
        if (!(await common_2.Helpers.compareHashPassword(data.password, user.password))) {
            throw new common_1.UnauthorizedException('Dados incorretos');
        }
        const token = await this.generateToken(user.id);
        const payload = {
            id: user.id,
            name: user.name,
            token,
        };
        user.accessToken = this.jwtService.sign(payload);
        return user;
    }
    async generateToken(id) {
        const { userTokenTable } = this.authConfigService.get();
        const token = common_2.Helpers.generateUserToken();
        try {
            const userTokens = await this.connection.collection(userTokenTable).insertOne({
                userId: id,
                token,
            });
        }
        catch (e) {
            if (e.code === 11000) {
                return this.generateToken(id);
            }
            throw e;
        }
        return token;
    }
};
AuthMongooseService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectConnection()),
    __metadata("design:paramtypes", [mongoose_2.Connection,
        config_service_1.AuthConfigService,
        jwt_1.JwtService])
], AuthMongooseService);
exports.AuthMongooseService = AuthMongooseService;
//# sourceMappingURL=auth-mongoose.service.js.map