"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const jwt_1 = require("@nestjs/jwt");
const common_2 = require("@devesharp/common");
const config_service_1 = require("./config.service");
const auth_abstract_service_1 = require("./auth.abstract.service");
let AuthTypeOrmService = class AuthTypeOrmService extends auth_abstract_service_1.AuthService {
    constructor(connection, authConfigService, jwtService) {
        super();
        this.connection = connection;
        this.authConfigService = authConfigService;
        this.jwtService = jwtService;
    }
    async validateUser(jwt) {
        const { userTokenTable } = this.authConfigService.get();
        const { usersTable } = this.authConfigService.get();
        const payload = this.jwtService.decode(jwt);
        if (!payload)
            return false;
        const userToken = await this.connection
            .createQueryBuilder(userTokenTable, userTokenTable)
            .orWhere(`${userTokenTable}.token = :token`, {
            token: payload.token,
        })
            .getOne();
        if (!userToken)
            throw new common_1.UnauthorizedException();
        const user = await await this.connection
            .createQueryBuilder(usersTable, usersTable)
            .orWhere(`${usersTable}.id = :id`, {
            id: userToken.userId,
        })
            .getOne();
        if (!user)
            throw new common_1.UnauthorizedException();
        return user;
    }
    async login(data) {
        const { usersTable } = this.authConfigService.get();
        const { usersLoginColumn } = this.authConfigService.get();
        const queryBuilder = this.connection.createQueryBuilder(usersTable, usersTable);
        usersLoginColumn.forEach(element => {
            queryBuilder.orWhere(`${usersTable}.${element} = :usersLoginColumn`, {
                usersLoginColumn: data.login,
            });
        });
        const user = await queryBuilder.getOne();
        if (!user) {
            throw new common_1.UnauthorizedException('Dados incorretos');
        }
        if (!(await common_2.Helpers.compareHashPassword(data.password, user.password))) {
            throw new common_1.UnauthorizedException('Dados incorretos');
        }
        const token = await this.generateToken(user.id);
        const payload = {
            id: user.id,
            name: user.name,
            token,
        };
        user.accessToken = this.jwtService.sign(payload);
        return user;
    }
    async generateToken(id) {
        const { userTokenTable } = this.authConfigService.get();
        const token = common_2.Helpers.generateUserToken();
        try {
            const userTokens = await this.connection
                .createQueryBuilder()
                .insert()
                .into(userTokenTable)
                .values([{ userId: id, token }])
                .execute();
        }
        catch (e) {
            if (e.errno === 1062) {
                return this.generateToken(id);
            }
            throw e;
        }
        return token;
    }
};
AuthTypeOrmService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectConnection()),
    __metadata("design:paramtypes", [typeorm_2.Connection,
        config_service_1.AuthConfigService,
        jwt_1.JwtService])
], AuthTypeOrmService);
exports.AuthTypeOrmService = AuthTypeOrmService;
//# sourceMappingURL=auth-typeorm.service.js.map