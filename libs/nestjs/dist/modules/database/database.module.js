"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var ConfigureDatabase_1, DatabaseModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const mongodb_providers_1 = require("./mongodb.providers");
const config_service_1 = require("./config.service");
const typeormConfigService_service_1 = require("./typeormConfigService.service");
let ConfigureDatabase = ConfigureDatabase_1 = class ConfigureDatabase {
    static forRoot(options) {
        return {
            module: ConfigureDatabase_1,
            providers: [
                config_service_1.AuthConfigService,
                {
                    provide: 'CONFIG_OPTIONS',
                    useValue: Object.assign(Object.assign({}, options), { database: 'mysql' }),
                },
                typeormConfigService_service_1.TypeOrmConfigService,
            ],
            exports: [typeormConfigService_service_1.TypeOrmConfigService],
        };
    }
};
ConfigureDatabase = ConfigureDatabase_1 = __decorate([
    common_1.Module({})
], ConfigureDatabase);
exports.ConfigureDatabase = ConfigureDatabase;
let DatabaseModule = DatabaseModule_1 = class DatabaseModule {
    static mongodb(options) {
        return {
            module: DatabaseModule_1,
            providers: [
                ...mongodb_providers_1.mongodbProviders,
                config_service_1.AuthConfigService,
                {
                    provide: 'CONFIG_OPTIONS',
                    useValue: Object.assign(Object.assign({}, options), { database: 'mongodb' }),
                },
            ],
            exports: [...mongodb_providers_1.mongodbProviders],
        };
    }
    static mysql(options) {
        return {
            module: DatabaseModule_1,
            imports: [
                typeorm_1.TypeOrmModule.forRootAsync({
                    imports: [ConfigureDatabase.forRoot(options)],
                    useExisting: typeormConfigService_service_1.TypeOrmConfigService,
                }),
            ],
            exports: [typeorm_1.TypeOrmModule],
        };
    }
};
DatabaseModule = DatabaseModule_1 = __decorate([
    common_1.Module({})
], DatabaseModule);
exports.DatabaseModule = DatabaseModule;
//# sourceMappingURL=database.module.js.map