"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const config_service_1 = require("./config.service");
exports.mongodbProviders = [
    {
        provide: 'MONGODB_CONNECTION',
        useFactory: async (authConfigService) => {
            let url = 'mongodb://';
            const { mongodbUser, mongodbPassword, mongodbHost, mongodbPort, mongodbDatabase } = authConfigService.get();
            if (mongodbUser && mongodbPassword) {
                url += `${mongodbUser}:${mongodbPassword}@`;
            }
            url += `${mongodbHost}:${mongodbPort}/`;
            url += mongodbDatabase;
            if (mongodbUser && mongodbPassword) {
                url += '?authSource=admin';
            }
            return mongoose.connect(url, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });
        },
        inject: [config_service_1.AuthConfigService],
    },
];
//# sourceMappingURL=mongodb.providers.js.map