"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const Joi = require("@hapi/joi");
let AuthConfigService = class AuthConfigService {
    constructor(options) {
        this.options = options;
        this.envConfig = this.validateInput(options);
    }
    validateInput(_config) {
        const config = _config;
        let envVarsSchema;
        if (config.database === 'mongodb') {
            delete config.database;
            envVarsSchema = Joi.object({
                mongodbHost: Joi.string().default('localhost'),
                mongodbDatabase: Joi.string().required(),
                mongodbPort: Joi.number().default(3000),
                mongodbUser: Joi.string().default(null),
                mongodbPassword: Joi.string().default(null),
                mongodbAuthSourge: Joi.string().default('admin'),
            });
        }
        else {
            delete config.database;
            envVarsSchema = Joi.object({
                mysqlType: Joi.string().default('mysql'),
                mysqlHost: Joi.string().default('localhost'),
                mysqlDatabase: Joi.string().required(),
                mysqlPort: Joi.number().default(3306),
                mysqlUsername: Joi.string().default(null),
                mysqlPassword: Joi.string().default(null),
                mysqlEntities: Joi.array().default([]),
                mysqlSynchronize: Joi.boolean().default(false),
            });
        }
        const { error, value: validatedConfig } = envVarsSchema.validate(config);
        if (error) {
            throw new Error(`DatabaseConfig validation error: ${error.message}`);
        }
        return validatedConfig;
    }
    get() {
        return this.envConfig;
    }
};
AuthConfigService = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject('CONFIG_OPTIONS')),
    __metadata("design:paramtypes", [Object])
], AuthConfigService);
exports.AuthConfigService = AuthConfigService;
//# sourceMappingURL=config.service.js.map