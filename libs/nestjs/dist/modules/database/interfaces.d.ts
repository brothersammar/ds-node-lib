import { EntitySchema } from 'typeorm';
export interface IDatabaseConfigMongoDB {
    mongodbHost?: string;
    mongodbDatabase?: string;
    mongodbPort?: number;
    mongodbUser?: string;
    mongodbPassword?: string;
    mongodbAuthSourge?: string;
}
export interface IDatabaseConfigMysql {
    mysqlType?: 'mysql' | 'mariadb';
    mysqlHost?: string;
    mysqlPort?: number;
    mysqlUsername?: string;
    mysqlPassword?: string;
    mysqlDatabase?: string;
    mysqlEntities?: (Function | string | EntitySchema<any>)[];
    mysqlSynchronize?: boolean;
}
