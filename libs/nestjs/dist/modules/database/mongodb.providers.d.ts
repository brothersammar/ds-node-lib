import * as mongoose from 'mongoose';
import { AuthConfigService } from './config.service';
export declare const mongodbProviders: {
    provide: string;
    useFactory: (authConfigService: AuthConfigService) => Promise<typeof mongoose>;
    inject: (typeof AuthConfigService)[];
}[];
