"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const config_service_1 = require("./config.service");
exports.mysqlProviders = [
    {
        provide: 'MYSQL_CONNECTION',
        useFactory: async (authConfigService) => await typeorm_1.createConnection({
            type: authConfigService.get().mysqlType,
            host: authConfigService.get().mysqlHost,
            port: authConfigService.get().mysqlPort,
            username: authConfigService.get().mysqlUsername,
            password: authConfigService.get().mysqlPassword,
            database: authConfigService.get().mysqlDatabase,
            entities: authConfigService.get().mysqlEntities,
            synchronize: authConfigService.get().mysqlSynchronize,
        }),
        inject: [config_service_1.AuthConfigService],
    },
];
//# sourceMappingURL=mysql.providers.js.map