import { TypeOrmOptionsFactory, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { AuthConfigService } from './config.service';
export declare class TypeOrmConfigService implements TypeOrmOptionsFactory {
    private authConfigService;
    constructor(authConfigService: AuthConfigService);
    createTypeOrmOptions(): TypeOrmModuleOptions;
}
