import { AuthConfigService } from './config.service';
export declare const mysqlProviders: {
    provide: string;
    useFactory: (authConfigService: AuthConfigService) => Promise<import("typeorm").Connection>;
    inject: (typeof AuthConfigService)[];
}[];
