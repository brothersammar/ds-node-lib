import { DynamicModule } from '@nestjs/common';
import { IDatabaseConfigMongoDB, IDatabaseConfigMysql } from './interfaces';
export declare class ConfigureDatabase {
    static forRoot(options: any): DynamicModule;
}
export declare class DatabaseModule {
    static mongodb(options?: IDatabaseConfigMongoDB): DynamicModule;
    static mysql(options?: IDatabaseConfigMysql): DynamicModule;
}
