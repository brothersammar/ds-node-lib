import { BaseEntity } from 'typeorm';
export declare class UsersTokenEntity extends BaseEntity {
    id: number;
    userId: number;
    token: string;
    lastUse: string;
    createdAt: string;
    updatedAt: string;
}
