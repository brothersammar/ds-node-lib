import { BaseEntity } from 'typeorm';
export declare class UsersEntity extends BaseEntity {
    id: number;
    enabled: boolean;
    name: string;
    password: string;
    email: string;
    login: string;
    role: number;
    lastAccess: Date;
    createAt: string;
    updateAt: string;
    deleteAt: Date;
}
