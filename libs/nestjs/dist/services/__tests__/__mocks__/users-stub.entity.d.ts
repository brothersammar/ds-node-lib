import { BaseEntity } from 'typeorm';
export declare class UsersStub extends BaseEntity {
    id?: number;
    enabled?: number;
    name?: string;
    password?: string;
    age: number;
    bloodType: number;
    deletedAt?: Date;
}
