interface IConfig {
    NODE_ENV: 'development' | 'production' | 'test' | 'provision';
    PORT: number;
    API_AUTH_ENABLED: boolean;
}
declare const _default: IConfig;
export default _default;
