"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Joi = require("@hapi/joi");
const config_service_1 = require("../../config.service");
const schema = Joi.object({
    NODE_ENV: Joi.string()
        .valid('development', 'production', 'test', 'provision')
        .default('development'),
    PORT: Joi.number().default(3000),
    API_AUTH_ENABLED: Joi.boolean().required(),
});
process.env.NODE_ENV = 'mock';
exports.default = new config_service_1.ConfigSetup(schema, `${__dirname}/`).getConfig();
//# sourceMappingURL=config-import-stub.js.map