import { Repository } from 'typeorm';
import { TypeOrmCrudManagerService } from '../../typeorm-crud-manager.service';
import { UsersStub } from './users-stub.entity';
export declare class UsersService extends TypeOrmCrudManagerService<UsersStub, any> {
    private readonly userRepository;
    constructor(userRepository: Repository<UsersStub>);
}
