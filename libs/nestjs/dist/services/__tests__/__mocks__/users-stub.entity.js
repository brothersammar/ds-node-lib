"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
let UsersStub = class UsersStub extends typeorm_1.BaseEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ type: 'bigint' }),
    __metadata("design:type", Number)
], UsersStub.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ default: 1 }),
    __metadata("design:type", Number)
], UsersStub.prototype, "enabled", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], UsersStub.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ default: '123456aa' }),
    __metadata("design:type", String)
], UsersStub.prototype, "password", void 0);
__decorate([
    typeorm_1.Column({ default: 1 }),
    __metadata("design:type", Number)
], UsersStub.prototype, "age", void 0);
__decorate([
    typeorm_1.Column({ default: 10 }),
    __metadata("design:type", Number)
], UsersStub.prototype, "bloodType", void 0);
__decorate([
    typeorm_1.Column({ type: 'datetime', nullable: true }),
    __metadata("design:type", Date)
], UsersStub.prototype, "deletedAt", void 0);
UsersStub = __decorate([
    typeorm_1.Entity('users')
], UsersStub);
exports.UsersStub = UsersStub;
//# sourceMappingURL=users-stub.entity.js.map