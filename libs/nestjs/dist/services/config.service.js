"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = require("dotenv");
class ConfigSetup {
    constructor(schema, dir) {
        let dirname = dir;
        if (!dirname) {
            dirname = ``;
        }
        this.file = `${process.env.NODE_ENV || ''}.env`;
        const config = dotenv.config({
            path: dirname + this.file,
        });
        if (config.error) {
            throw new Error(config.error.toString());
        }
        this.envConfig = this.validate(config.parsed, schema);
    }
    validate(envConfig, schema) {
        const envVarsSchema = schema;
        const { error, value: validatedEnvConfig } = envVarsSchema.validate(envConfig);
        if (error) {
            throw new Error(`${this.file} validation error: ${error.message}`);
        }
        return validatedEnvConfig;
    }
    getConfig() {
        return this.envConfig;
    }
}
exports.ConfigSetup = ConfigSetup;
//# sourceMappingURL=config.service.js.map