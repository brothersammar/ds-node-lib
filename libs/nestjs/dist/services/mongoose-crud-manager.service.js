"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const date_fns_1 = require("date-fns");
const common_1 = require("@nestjs/common");
const common_2 = require("@devesharp/common");
class MongooseCrudManager {
    constructor(model) {
        this.sort = [];
        this.sortDefault = '';
        this.limitMax = 100;
        this.limitDefault = 20;
        this.filters = {};
        this.fields = [];
        this.query = {};
        this.aliases = [];
        this.aliasesAllowed = [];
        this.model = model;
    }
    verifyAuthorization(method, args) {
        return true;
    }
    beforeDataHandling(method, data, args) {
        return data;
    }
    async beforeModelHandlingAction(method, model, args) {
    }
    async searchHandlingQueryBuilder(query, args) {
    }
    async create(_data) {
        if (!this.verifyAuthorization('create', { data: _data })) {
            throw new common_1.UnauthorizedException();
        }
        const data = await this.beforeDataHandling('create', _data, {});
        let model = new this.model(data);
        await this.beforeModelHandlingAction('create', model, {
            data,
            model,
        });
        model = await model.save();
        return model;
    }
    async get(_id) {
        const model = await this.__get(_id);
        if (!model)
            throw new common_1.NotFoundException();
        if (!this.verifyAuthorization('get', { model: model, data: _id })) {
            throw new common_1.UnauthorizedException();
        }
        return model;
    }
    async update(_id, data) {
        const model = await this.__get(_id);
        if (!this.verifyAuthorization('update', {
            model: model,
            data: _id,
        })) {
            throw new common_1.UnauthorizedException();
        }
        data = await this.beforeDataHandling('update', data, {});
        return this.__update(model.id, data);
    }
    async delete(_id) {
        const model = await this.__get(_id);
        if (!this.verifyAuthorization('delete', {
            model: model,
            data: _id,
        })) {
            throw new common_1.UnauthorizedException();
        }
        let data = {
            enabled: 0,
            deleted_at: date_fns_1.format(new Date(), 'yyyy-MM-dd hh:mm:ss'),
        };
        data = await this.beforeDataHandling('search', data, {
            data,
            model,
        });
        return this.__update(model.id, data);
    }
    async search(data) {
        if (!this.verifyAuthorization('search', {})) {
            throw new common_1.UnauthorizedException();
        }
        data = await this.beforeDataHandling('search', data, {});
        const queryBuilder = this.__search(data.filters, data.query, data.alias);
        await this.searchHandlingQueryBuilder(queryBuilder, {
            data,
        });
        const results = {
            count: 0,
            results: [],
        };
        if (data && data.query && data.query.limit == 0) {
            results.results = [];
        }
        else {
            results.results = await queryBuilder;
        }
        results.count = await queryBuilder.countDocuments();
        return results;
    }
    async __get(_id) {
        const model = await this.model.findOne({
            _id,
            enabled: 1,
        });
        return Promise.resolve(model);
    }
    async __update(_id, body) {
        const modelUpdate = await this.model.updateOne({
            _id,
            enabled: 1,
        }, body);
        return !!modelUpdate.ok;
    }
    __search(filters, query, alias) {
        let queryBuilder = this.model.find();
        if (!query)
            query = {};
        if (!filters)
            filters = {};
        queryBuilder = this.__filters(queryBuilder, filters || {});
        return queryBuilder;
    }
    __query(resource, query) {
        let limit = this.limitDefault;
        if (query.limit && !Number.isNaN(Number(query.limit)) && query.limit > 0) {
            resource.limit(Math.min(this.limitMax, Number(query.limit)));
            limit = Math.min(this.limitMax, Number(query.limit));
        }
        else {
            resource.limit(this.limitDefault);
        }
        if (query.offset && !Number.isNaN(Number(query.offset)) && query.offset > 0) {
            resource.skip(Number(query.offset));
        }
        if (query.page && !Number.isNaN(Number(query.page)) && query.page > 0) {
            resource.skip((Number(query.page) - 1) * limit);
        }
        return resource;
    }
    __projection(resource, projections) {
        const newProj = {};
        if (projections.length > 0) {
            projections.forEach(element => {
                if (typeof element === 'string') {
                    newProj[element] = 1;
                }
            });
            if (this.aliases.length > 0) {
                this.aliases.forEach(element => {
                    if (typeof element === 'string') {
                        newProj[element] = 1;
                    }
                });
            }
            resource.select(newProj);
        }
        return resource;
    }
    __sorts(resource, sortVal) {
        const data = {
            sort: {},
        };
        for (let i = 0; i < this.sort.length; i++) {
            const mainSort = this.sort[i];
            for (let iS = 0; iS < sortVal.length; iS++) {
                const reqSort = sortVal[iS];
                let checkSort = sortVal[iS].toString();
                if (typeof mainSort === 'string') {
                    if (reqSort.indexOf('-') === 0) {
                        checkSort = reqSort.substring(1);
                    }
                    if (checkSort === mainSort) {
                        if (reqSort.indexOf('-') === 0) {
                            data.sort[checkSort] = -1;
                        }
                        else {
                            data.sort[checkSort] = 1;
                        }
                    }
                }
                else if (typeof mainSort === 'object') {
                    if (reqSort.indexOf('-') === 0) {
                        checkSort = reqSort.substring(1);
                    }
                    if (mainSort.name === checkSort) {
                        if (reqSort.indexOf('-') === 0) {
                            data.sort[mainSort.alias] = -1;
                        }
                        else {
                            data.sort[mainSort.alias] = 1;
                        }
                    }
                }
            }
        }
        if (this.sort.length === 0 || (Object.entries(data.sort).length === 0 && data.sort.constructor === Object)) {
            data.sort = this.sortDefault;
        }
        return resource.find().setOptions(data);
    }
    __filters(resource, filters) {
        for (const key in this.filters) {
            if (this.filters.hasOwnProperty(key)) {
                const filterConfig = this.filters[key];
                for (const key2 in filters) {
                    if (filters.hasOwnProperty(key2)) {
                        const filterItem = filters[key2];
                        if (key === key2) {
                            switch (filterConfig.filter) {
                                case 'int':
                                    resource.where(filterConfig.column, Number(filterItem));
                                    break;
                                case 'intGt':
                                    resource.gt(filterConfig.column, Number(filterItem));
                                    break;
                                case 'intLt':
                                    resource.lt(filterConfig.column, Number(filterItem));
                                    break;
                                case 'intGte':
                                    resource.gte(filterConfig.column, Number(filterItem));
                                    break;
                                case 'intLte':
                                    resource.lte(filterConfig.column, Number(filterItem));
                                    break;
                                case 'string':
                                    resource.where(filterConfig.column, filterItem);
                                    break;
                                case 'stringLike':
                                    resource.where(filterConfig.column, {
                                        $regex: `^${filterItem}$`,
                                        $options: 'i',
                                    });
                                    break;
                                case 'stringLikeNot':
                                    resource.where(filterConfig.column, {
                                        $regex: `^((?!${filterItem}).)*$`,
                                        $options: 'i',
                                    });
                                    break;
                                case 'arrayInt':
                                    if (common_2.Helpers.typeOf(filterItem) === 'array') {
                                        resource.in(filterConfig.column, filterItem.map(Number));
                                    }
                                    break;
                                case 'arrayIntNot':
                                    if (common_2.Helpers.typeOf(filterItem) === 'array') {
                                        resource.nin(filterConfig.column, filterItem.map(Number));
                                    }
                                    break;
                                case 'arrayString':
                                    if (common_2.Helpers.typeOf(filterItem) === 'array') {
                                        resource.in(filterConfig.column, filterItem);
                                    }
                                    break;
                                case 'arrayStringNot':
                                    if (common_2.Helpers.typeOf(filterItem) === 'array') {
                                        resource.nin(filterConfig.column, filterItem);
                                    }
                                    break;
                                case 'stringLikePercent':
                                    resource.where(filterConfig.column, {
                                        $regex: filterItem,
                                        $options: 'i',
                                    });
                                    break;
                                case 'stringLikePercentAfter':
                                    resource.where(filterConfig.column, {
                                        $regex: `${filterItem}$`,
                                        $options: 'i',
                                    });
                                    break;
                                case 'stringLikePercentBefore':
                                    resource.where(filterConfig.column, {
                                        $regex: `^${filterItem}`,
                                        $options: 'i',
                                    });
                                    break;
                            }
                        }
                    }
                }
            }
        }
        return resource;
    }
}
exports.MongooseCrudManager = MongooseCrudManager;
//# sourceMappingURL=mongoose-crud-manager.service.js.map