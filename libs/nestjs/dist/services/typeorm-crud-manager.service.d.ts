import { BaseEntity, EntityManager, SelectQueryBuilder, Repository } from 'typeorm';
import { IBodySearch, IResultSearch, IArgsMethod, IFilter, ISortSet, IQuery } from '../interfaces/crud-interfaces.interface';
export declare class TypeOrmCrudManagerService<T extends BaseEntity, AuthClass> {
    auth: AuthClass | boolean;
    sort: (ISortSet | string)[];
    sortDefault: string;
    limitMax: number;
    limitDefault: number;
    filters: {
        [key: string]: IFilter;
    };
    fields: any[];
    query: {};
    aliases: any[];
    aliasesAllowed: any[];
    transaction: boolean;
    transactionalEntityManager: EntityManager;
    instance: Repository<T>;
    entityManager: EntityManager;
    constructor(instance: Repository<T>);
    addAuth(auth?: AuthClass | boolean): this;
    verifyAuthorization(method: 'create' | 'get' | 'update' | 'search' | 'delete', args: IArgsMethod<T>): boolean;
    beforeDataHandling(method: 'create' | 'get' | 'update' | 'search' | 'delete', data: any, args: IArgsMethod<T>): Promise<any>;
    beforeModelHandlingAction(method: 'create' | 'get' | 'update' | 'search' | 'delete', model: T, args: IArgsMethod<T>): Promise<any>;
    searchHandlingQueryBuilder(query: SelectQueryBuilder<any>, args: IArgsMethod<T>): Promise<any>;
    create<B>(_data: B): Promise<any>;
    get(id: number): Promise<any>;
    update<B>(id: number, _data: B): Promise<boolean>;
    delete(id: number): Promise<boolean>;
    search<B, B2>(_data: IBodySearch<B>): Promise<IResultSearch<B2>>;
    protected getModel(id: number): Promise<T>;
    protected updateModel(id: number, body: any): Promise<boolean>;
    protected __search(_filters: any, _query: any, alias: string[]): SelectQueryBuilder<any>;
    private __getRawMany;
    __query(resource: SelectQueryBuilder<any>, query: IQuery): SelectQueryBuilder<any>;
    __projection(resource: SelectQueryBuilder<any>, projections: string[]): SelectQueryBuilder<any>;
    __sorts(resource: SelectQueryBuilder<any>, sortVal: string[]): SelectQueryBuilder<any>;
    __count(resource: SelectQueryBuilder<any>): Promise<number>;
    __filters(resource: SelectQueryBuilder<any>, filters: {
        [key: string]: any;
    }): SelectQueryBuilder<any>;
}
