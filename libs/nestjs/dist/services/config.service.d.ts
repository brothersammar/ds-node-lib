import * as Joi from '@hapi/joi';
export declare class ConfigSetup<T> {
    private file;
    private envConfig;
    constructor(schema: Joi.ObjectSchema, dir?: string);
    validate(envConfig: any, schema: Joi.ObjectSchema): T;
    getConfig(): T;
}
