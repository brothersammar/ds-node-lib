"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const date_fns_1 = require("date-fns");
const common_1 = require("@nestjs/common");
const common_2 = require("@devesharp/common");
class TypeOrmCrudManagerService {
    constructor(instance) {
        this.auth = false;
        this.sort = [];
        this.sortDefault = '';
        this.limitMax = 100;
        this.limitDefault = 20;
        this.filters = {};
        this.fields = [];
        this.query = {};
        this.aliases = [];
        this.aliasesAllowed = [];
        this.transaction = false;
        this.instance = instance;
        this.entityManager = typeorm_1.getConnection().manager;
    }
    addAuth(auth) {
        this.auth = auth || false;
        return this;
    }
    verifyAuthorization(method, args) {
        return true;
    }
    beforeDataHandling(method, data, args) {
        return data;
    }
    async beforeModelHandlingAction(method, model, args) {
    }
    async searchHandlingQueryBuilder(query, args) {
    }
    async create(_data) {
        if (!this.verifyAuthorization('create', { data: _data })) {
            throw new common_1.UnauthorizedException();
        }
        const data = await this.beforeDataHandling('create', _data, {});
        const model = this.instance.create(data);
        await this.beforeModelHandlingAction('create', model, {
            data,
            model,
        });
        return this.entityManager.save(model);
    }
    async get(id) {
        const model = await this.instance.findOne({
            where: {
                id,
                enabled: 1,
            },
        });
        if (!model)
            throw new common_1.NotFoundException();
        if (!this.verifyAuthorization('get', { model: model, data: id })) {
            throw new common_1.UnauthorizedException();
        }
        return model;
    }
    async update(id, _data) {
        const model = await this.getModel(id);
        if (!this.verifyAuthorization('update', { model: model, data: id })) {
            throw new common_1.UnauthorizedException();
        }
        const data = await this.beforeDataHandling('update', _data, {
            data: _data,
            model,
        });
        return this.updateModel(model.id, data);
    }
    async delete(id) {
        const model = await this.getModel(id);
        if (!this.verifyAuthorization('delete', { model: model, data: id })) {
            throw new common_1.UnauthorizedException();
        }
        let data = {
            enabled: 0,
            deletedAt: date_fns_1.format(new Date(), 'yyyy-MM-dd hh:mm:ss'),
        };
        data = await this.beforeDataHandling('search', data, {
            data,
            model,
        });
        return this.updateModel(model.id, data);
    }
    async search(_data) {
        if (!this.verifyAuthorization('search', {})) {
            throw new common_1.UnauthorizedException();
        }
        const data = await this.beforeDataHandling('search', _data, {});
        const queryBuilder = this.__search(data.filters, data.query, data.alias);
        await this.searchHandlingQueryBuilder(queryBuilder, {
            data,
        });
        const results = {
            count: 0,
            results: [],
        };
        if (data && data.query && data.query.limit === 0) {
            results.results = [];
        }
        else {
            results.results = await this.__getRawMany(queryBuilder);
        }
        results.count = await queryBuilder.getCount();
        return results;
    }
    async getModel(id) {
        const model = await this.instance.findOne({
            where: {
                id,
                enabled: 1,
            },
        });
        return Promise.resolve(model);
    }
    async updateModel(id, body) {
        const modelUpdate = await this.entityManager.update(this.instance.metadata.name, id, body);
        return Promise.resolve(!!modelUpdate.raw.affectedRows);
    }
    __search(_filters, _query, alias) {
        let queryBuilder = this.instance.createQueryBuilder();
        let query = _query;
        let filters = _filters;
        if (!query)
            query = {};
        if (!filters)
            filters = {};
        queryBuilder = this.__filters(queryBuilder, filters || {});
        queryBuilder = this.__query(queryBuilder, query || {});
        queryBuilder = this.__sorts(queryBuilder, query.sorts || []);
        queryBuilder = this.__projection(queryBuilder, alias || []);
        return queryBuilder;
    }
    async __getRawMany(documentQuery) {
        const results = await documentQuery.getRawMany();
        const newResults = [];
        results.forEach(row => {
            const newRow = {};
            Object.entries(row).forEach(values => {
                const [key, value] = values;
                newRow[key.replace(`${this.instance.metadata.name}_`, '')] = value;
            });
            newResults.push(newRow);
        });
        return newResults;
    }
    __query(resource, query) {
        let limit = this.limitDefault;
        if (query.limit && !Number.isNaN(Number(query.limit)) && query.limit > 0) {
            resource.limit(Math.min(this.limitMax, Number(query.limit)));
            limit = Math.min(this.limitMax, Number(query.limit));
        }
        else {
            resource.limit(this.limitDefault);
        }
        if (query.offset && !Number.isNaN(Number(query.offset)) && query.offset > 0) {
            resource.offset(Number(query.offset));
        }
        if (query.page && !Number.isNaN(Number(query.page)) && query.page > 0) {
            resource.offset((Number(query.page) - 1) * limit);
        }
        return resource;
    }
    __projection(resource, projections) {
        const newProj = [];
        if (projections.length > 0) {
            projections.forEach(element => {
                if (typeof element === 'string' &&
                    (this.aliasesAllowed.indexOf(element) !== -1 || !this.aliasesAllowed.length)) {
                    newProj.push(`\`${this.instance.metadata.name.toString()}\`.\`${element}\``);
                }
            });
            if (this.aliases.length > 0) {
                this.aliases.forEach(element => {
                    if (typeof element === 'string') {
                        newProj.push(`\`${this.instance.metadata.name.toString()}\`.\`${element}\``);
                    }
                });
            }
            resource = resource.select(newProj);
        }
        return resource;
    }
    __sorts(resource, sortVal) {
        const data = {
            sort: {},
        };
        for (let i = 0; i < this.sort.length; i += 1) {
            const mainSort = this.sort[i];
            for (let iS = 0; iS < sortVal.length; iS += 1) {
                const reqSort = sortVal[iS];
                let checkSort = sortVal[iS].toString();
                if (typeof mainSort === 'string') {
                    if (reqSort.indexOf('-') === 0) {
                        checkSort = reqSort.substring(1);
                    }
                    if (checkSort === mainSort) {
                        if (reqSort.indexOf('-') === 0) {
                            resource.addOrderBy(checkSort, 'DESC');
                        }
                        else {
                            resource.addOrderBy(checkSort, 'ASC');
                        }
                    }
                }
                else if (typeof mainSort === 'object') {
                    if (reqSort.indexOf('-') === 0) {
                        checkSort = reqSort.substring(1);
                    }
                    if (mainSort.name === checkSort) {
                        if (reqSort.indexOf('-') === 0) {
                            resource.addOrderBy(mainSort.alias, 'DESC');
                        }
                        else {
                            resource.addOrderBy(mainSort.alias, 'ASC');
                        }
                    }
                }
            }
        }
        if (sortVal.length === 0 && this.sortDefault !== '') {
            if (this.sortDefault.indexOf('-') === 0) {
                resource.addOrderBy(this.sortDefault.substr(1), 'DESC');
            }
            else {
                resource.addOrderBy(this.sortDefault, 'ASC');
            }
        }
        return resource;
    }
    __count(resource) {
        return resource
            .limit()
            .offset(0)
            .getCount();
    }
    __filters(resource, filters) {
        Object.entries(this.filters).forEach(valueThisFilters => {
            const [key, filterConfig] = valueThisFilters;
            Object.entries(filters).forEach(valueFilters => {
                const [key2, filterItem] = valueFilters;
                if (key === key2) {
                    switch (filterConfig.filter) {
                        case 'int':
                            resource.andWhere(`${filterConfig.column} = :${key}`, {
                                [key]: Number(filterItem),
                            });
                            break;
                        case 'intGt':
                            resource.andWhere(`${filterConfig.column} > :${key}`, {
                                [key]: Number(filterItem),
                            });
                            break;
                        case 'intLt':
                            resource.andWhere(`${filterConfig.column} < :${key}`, {
                                [key]: Number(filterItem),
                            });
                            break;
                        case 'intGte':
                            resource.andWhere(`${filterConfig.column} >= :${key}`, {
                                [key]: Number(filterItem),
                            });
                            break;
                        case 'intLte':
                            resource.andWhere(`${filterConfig.column} <= :${key}`, {
                                [key]: Number(filterItem),
                            });
                            break;
                        case 'string':
                            resource.andWhere(`${filterConfig.column} = :${key}`, {
                                [key]: filterItem,
                            });
                            break;
                        case 'stringLike':
                            resource.andWhere(`${filterConfig.column} LIKE :${key}`, {
                                [key]: filterItem,
                            });
                            break;
                        case 'arrayInt':
                            if (common_2.Helpers.typeOf(filterItem) === 'array') {
                                resource.andWhere(`${filterConfig.column} IN (:${key})`, {
                                    [key]: filterItem.map(Number),
                                });
                            }
                            break;
                        case 'arrayIntNot':
                            if (common_2.Helpers.typeOf(filterItem) === 'array') {
                                resource.andWhere(`${filterConfig.column} NOT IN (:${key})`, {
                                    [key]: filterItem.map(Number),
                                });
                            }
                            break;
                        case 'arrayString':
                            if (common_2.Helpers.typeOf(filterItem) === 'array') {
                                resource.andWhere(`${filterConfig.column} IN (:${key})`, {
                                    [key]: filterItem,
                                });
                            }
                            break;
                        case 'arrayStringNot':
                            if (common_2.Helpers.typeOf(filterItem) === 'array') {
                                resource.andWhere(`${filterConfig.column} NOT IN (:${key})`, {
                                    [key]: filterItem,
                                });
                            }
                            break;
                        case 'stringLikeNot':
                            resource.andWhere(`${filterConfig.column} NOT LIKE :${key}`, {
                                [key]: filterItem,
                            });
                            break;
                        case 'stringLikePercent':
                            resource.andWhere(`${filterConfig.column} LIKE :${key}`, {
                                [key]: `%${filterItem}%`,
                            });
                            break;
                        case 'stringLikePercentAfter':
                            resource.andWhere(`${filterConfig.column} LIKE :${key}`, {
                                [key]: `${filterItem}%`,
                            });
                            break;
                        case 'stringLikePercentBefore':
                            resource.andWhere(`${filterConfig.column} LIKE :${key}`, {
                                [key]: `%${filterItem}`,
                            });
                            break;
                        case 'intInColumn':
                            if (common_2.Helpers.typeOf(filterItem) === 'array') {
                                resource.andWhere(`FIND_IN_SET(:${key}, ${filterConfig.column}) = 0`, {
                                    [key]: filterItem.map(Number),
                                });
                            }
                            break;
                        default:
                            throw Error(`${filterConfig.filter} filter not defined`);
                    }
                }
            });
        });
        return resource;
    }
}
exports.TypeOrmCrudManagerService = TypeOrmCrudManagerService;
//# sourceMappingURL=typeorm-crud-manager.service.js.map