import { Model, Document, DocumentQuery } from 'mongoose';
import { IBodySearch, IResultSearch, IArgsMethod, IFilter, ISortSet, IQuery } from '../interfaces/crud-interfaces.interface';
export declare class MongooseCrudManager<T extends Document> {
    sort: (ISortSet | string)[];
    sortDefault: string;
    limitMax: number;
    limitDefault: number;
    filters: {
        [key: string]: IFilter;
    };
    fields: any[];
    query: {};
    aliases: any[];
    aliasesAllowed: any[];
    model: typeof Model;
    constructor(model: typeof Model);
    verifyAuthorization(method: 'create' | 'get' | 'update' | 'search' | 'delete', args: IArgsMethod<T>): boolean;
    beforeDataHandling(method: 'create' | 'get' | 'update' | 'search' | 'delete', data: any, args: IArgsMethod<T>): Promise<any>;
    beforeModelHandlingAction(method: 'create' | 'get' | 'update' | 'search' | 'delete', model: T, args: IArgsMethod<T>): Promise<any>;
    searchHandlingQueryBuilder(query: DocumentQuery<any, any>, args: IArgsMethod<T>): Promise<any>;
    create<B>(_data: B): Promise<any>;
    get(_id: string): Promise<any>;
    update<B>(_id: string, data: B): Promise<boolean>;
    delete(_id: string): Promise<boolean>;
    search<B, B2>(data: IBodySearch<B>): Promise<IResultSearch<B2>>;
    __get(_id: string): Promise<T>;
    __update(_id: string, body: any): Promise<boolean>;
    protected __search(filters: any, query: any, alias: string[]): DocumentQuery<any, any>;
    __query(resource: DocumentQuery<any, any>, query: IQuery): DocumentQuery<any, any>;
    __projection(resource: DocumentQuery<any, any>, projections: string[]): DocumentQuery<any, any>;
    __sorts(resource: DocumentQuery<any, any>, sortVal: string[]): DocumentQuery<any, any>;
    __filters(resource: DocumentQuery<any, any>, filters: {
        [key: string]: any;
    }): DocumentQuery<any, any>;
}
