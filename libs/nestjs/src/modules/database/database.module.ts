import { Module, DynamicModule } from '@nestjs/common';
import { TypeOrmModule, TypeOrmModuleAsyncOptions } from '@nestjs/typeorm';
import { mongodbProviders } from './mongodb.providers';
import { mysqlProviders } from './mysql.providers';
import { IDatabaseConfigMongoDB, IDatabaseConfigMysql } from './interfaces';
import { AuthConfigService } from './config.service';
import { TypeOrmConfigService } from './typeormConfigService.service';

@Module({})
export class ConfigureDatabase {
    static forRoot(options): DynamicModule {
        return {
            module: ConfigureDatabase,
            providers: [
                AuthConfigService,
                {
                    provide: 'CONFIG_OPTIONS',
                    useValue: { ...options, ...{ database: 'mysql' } },
                },
                TypeOrmConfigService,
            ],
            exports: [TypeOrmConfigService],
        };
    }
}

@Module({})
export class DatabaseModule {
    static mongodb(options?: IDatabaseConfigMongoDB): DynamicModule {
        return {
            module: DatabaseModule,
            providers: [
                ...mongodbProviders,
                AuthConfigService,
                {
                    provide: 'CONFIG_OPTIONS',
                    useValue: { ...options, ...{ database: 'mongodb' } },
                },
            ],
            exports: [...mongodbProviders],
        };
    }

    static mysql(options?: IDatabaseConfigMysql): DynamicModule {
        return {
            module: DatabaseModule,
            imports: [
                TypeOrmModule.forRootAsync({
                    imports: [ConfigureDatabase.forRoot(options)],
                    useExisting: TypeOrmConfigService,
                }),
            ],
            exports: [TypeOrmModule],
        };
    }
}
