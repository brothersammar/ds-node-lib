import * as mongoose from 'mongoose';
import { AuthConfigService } from './config.service';
import { IDatabaseConfigMongoDB } from './interfaces';

export const mongodbProviders = [
    {
        provide: 'MONGODB_CONNECTION',
        useFactory: async (authConfigService: AuthConfigService): Promise<typeof mongoose> => {
            let url = 'mongodb://';
            const { mongodbUser, mongodbPassword, mongodbHost, mongodbPort, mongodbDatabase } = authConfigService.get<
                IDatabaseConfigMongoDB
            >();

            if (mongodbUser && mongodbPassword) {
                url += `${mongodbUser}:${mongodbPassword}@`;
            }

            url += `${mongodbHost}:${mongodbPort}/`;
            url += mongodbDatabase;

            if (mongodbUser && mongodbPassword) {
                url += '?authSource=admin';
            }

            return mongoose.connect(url, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });
        },
        inject: [AuthConfigService],
    },
];
