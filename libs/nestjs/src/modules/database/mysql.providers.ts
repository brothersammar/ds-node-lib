import * as mongoose from 'mongoose';
import { createConnection } from 'typeorm';
import { AuthConfigService } from './config.service';
import { IDatabaseConfigMysql } from './interfaces';

export const mysqlProviders = [
    {
        provide: 'MYSQL_CONNECTION',
        useFactory: async (authConfigService: AuthConfigService) =>
            await createConnection({
                type: authConfigService.get<IDatabaseConfigMysql>().mysqlType,
                host: authConfigService.get<IDatabaseConfigMysql>().mysqlHost,
                port: authConfigService.get<IDatabaseConfigMysql>().mysqlPort,
                username: authConfigService.get<IDatabaseConfigMysql>().mysqlUsername,
                password: authConfigService.get<IDatabaseConfigMysql>().mysqlPassword,
                database: authConfigService.get<IDatabaseConfigMysql>().mysqlDatabase,
                entities: authConfigService.get<IDatabaseConfigMysql>().mysqlEntities,
                synchronize: authConfigService.get<IDatabaseConfigMysql>().mysqlSynchronize,
            }),
        inject: [AuthConfigService],
    },
];
