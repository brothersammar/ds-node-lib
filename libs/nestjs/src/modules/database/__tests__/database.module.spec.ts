import * as mongoose from 'mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { ConnectionOptions, getConnection } from 'typeorm';
import { connection } from 'mongoose';
import { DatabaseModule } from '../database.module';

describe('Auth Controller TypeOrm', () => {
    let databaseModule: DatabaseModule;

    afterEach(async () => {
        try {
            await getConnection().close();
        } catch (error) {}
    });

    it('deve criar conexão com mongodb', async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                DatabaseModule.mongodb({
                    mongodbHost: 'localhost',
                    mongodbPort: 9312,
                    mongodbDatabase: 'testing',
                    mongodbUser: 'admin',
                    mongodbPassword: '123456aa',
                }),
            ],
        }).compile();

        databaseModule = module.get<DatabaseModule>(DatabaseModule);

        expect(databaseModule).toBeDefined();
    });

    it('deve criar conexão com mysql', async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                DatabaseModule.mysql({
                    mysqlHost: 'localhost',
                    mysqlPort: 9311,
                    mysqlUsername: 'root',
                    mysqlPassword: '123456aa',
                    mysqlDatabase: 'testing',
                }),
            ],
        }).compile();

        databaseModule = module.get<DatabaseModule>(DatabaseModule);

        expect(databaseModule).toBeDefined();
    });

    it('deve criar conexão com mysql e mongodb', async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                DatabaseModule.mysql({
                    mysqlHost: 'localhost',
                    mysqlPort: 9311,
                    mysqlUsername: 'root',
                    mysqlPassword: '123456aa',
                    mysqlDatabase: 'testing',
                }),
                DatabaseModule.mongodb({
                    mongodbHost: 'localhost',
                    mongodbPort: 9312,
                    mongodbDatabase: 'testing',
                    mongodbUser: 'admin',
                    mongodbPassword: '123456aa',
                }),
            ],
        }).compile();

        databaseModule = module.get<DatabaseModule>(DatabaseModule);

        expect(databaseModule).toBeDefined();
    });
});
