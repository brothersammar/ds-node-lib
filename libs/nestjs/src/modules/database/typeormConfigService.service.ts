import { Injectable } from '@nestjs/common';
import { TypeOrmOptionsFactory, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { AuthConfigService } from './config.service';
import { IDatabaseConfigMongoDB, IDatabaseConfigMysql } from './interfaces';

@Injectable()
export class TypeOrmConfigService implements TypeOrmOptionsFactory {
    constructor(private authConfigService: AuthConfigService) {}

    createTypeOrmOptions(): TypeOrmModuleOptions {
        return {
            type: 'mysql',
            host: this.authConfigService.get<IDatabaseConfigMysql>().mysqlHost,
            port: this.authConfigService.get<IDatabaseConfigMysql>().mysqlPort,
            username: this.authConfigService.get<IDatabaseConfigMysql>().mysqlUsername,
            password: this.authConfigService.get<IDatabaseConfigMysql>().mysqlPassword,
            database: this.authConfigService.get<IDatabaseConfigMysql>().mysqlDatabase,
            entities: this.authConfigService.get<IDatabaseConfigMysql>().mysqlEntities,
            synchronize: this.authConfigService.get<IDatabaseConfigMysql>().mysqlSynchronize,
            keepConnectionAlive: true,
        };
    }
}
