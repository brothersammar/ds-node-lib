import { Injectable, Inject } from '@nestjs/common';
import * as Joi from '@hapi/joi';

@Injectable()
export class AuthConfigService {
    private readonly envConfig: any;

    constructor(@Inject('CONFIG_OPTIONS') private options) {
        this.envConfig = this.validateInput(options);
    }

    private validateInput(_config: any): any {
        const config = _config;

        let envVarsSchema: Joi.ObjectSchema;
        if (config.database === 'mongodb') {
            delete config.database;
            envVarsSchema = Joi.object({
                // MongoDB
                mongodbHost: Joi.string().default('localhost'),
                mongodbDatabase: Joi.string().required(),
                mongodbPort: Joi.number().default(3000),
                mongodbUser: Joi.string().default(null),
                mongodbPassword: Joi.string().default(null),
                mongodbAuthSourge: Joi.string().default('admin'),
            });
        } else {
            delete config.database;
            envVarsSchema = Joi.object({
                // Mysql
                mysqlType: Joi.string().default('mysql'),
                mysqlHost: Joi.string().default('localhost'),
                mysqlDatabase: Joi.string().required(),
                mysqlPort: Joi.number().default(3306),
                mysqlUsername: Joi.string().default(null),
                mysqlPassword: Joi.string().default(null),
                mysqlEntities: Joi.array().default([]),
                mysqlSynchronize: Joi.boolean().default(false),
            });
        }

        const { error, value: validatedConfig } = envVarsSchema.validate(config);

        if (error) {
            throw new Error(`DatabaseConfig validation error: ${error.message}`);
        }
        return validatedConfig;
    }

    get<T>(): T {
        return this.envConfig;
    }
}
