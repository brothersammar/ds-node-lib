import { CanActivate, ExecutionContext, Injectable, UnauthorizedException, Inject } from '@nestjs/common';
import { AuthService } from './auth.abstract.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(@Inject('AuthService') private readonly authService: AuthService) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();

        if (request.headers.authorization) {
            const token = (request.headers.authorization as string).split(' ')[1] || null;

            const user = await this.authService.validateUser(token);

            // Não autorizado
            if (!user) {
                throw new UnauthorizedException();
            }

            request.user = user;

            return true;
        }

        // Não autorizado
        throw new UnauthorizedException();
    }
}
