import { Injectable, Inject } from '@nestjs/common';
import * as Joi from '@hapi/joi';
import { IAuthConfig } from './interfaces';

@Injectable()
export class AuthConfigService {
    private readonly envConfig: any;

    constructor(@Inject('CONFIG_OPTIONS') private options) {
        this.envConfig = this.validateInput(options);
    }

    private validateInput(config: any): any {
        const envVarsSchema: Joi.ObjectSchema = Joi.object({
            secretKey: Joi.string().required(),
            usersTable: Joi.string().default('users'),
            usersLoginColumn: Joi.array().default(['login', 'email']),
            usersPasswordColumn: Joi.string().default('password'),
            userTokenTable: Joi.string().default('users_tokens'),
        });

        const { error, value: validatedConfig } = envVarsSchema.validate(config);

        if (error) {
            throw new Error(`AuthConfig validation error: ${error.message}`);
        }
        return validatedConfig;
    }

    get(): IAuthConfig {
        return this.envConfig;
    }
}
