import { Injectable, Inject, UnauthorizedException } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import { createQueryBuilder, Connection } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { Helpers } from '@devesharp/common';
import { AuthDto } from './dto/auth-dto';
import { AuthConfigService } from './config.service';
import { AuthService } from './auth.abstract.service';

@Injectable()
export class AuthTypeOrmService extends AuthService {
    constructor(
        @InjectConnection() private connection: Connection,
        private readonly authConfigService: AuthConfigService,
        private readonly jwtService: JwtService,
    ) {
        super();
    }

    /**
     * Valida token e retorna usuário
     * @param jwt
     */
    async validateUser(jwt: string): Promise<any> {
        const { userTokenTable } = this.authConfigService.get();
        const { usersTable } = this.authConfigService.get();

        // Payload
        const payload: any = this.jwtService.decode(jwt);
        if (!payload) return false;

        // Verificar token
        const userToken: any = await this.connection
            .createQueryBuilder(userTokenTable, userTokenTable)
            .orWhere(`${userTokenTable}.token = :token`, {
                token: payload.token,
            })
            .getOne();

        if (!userToken) throw new UnauthorizedException();

        // Usuário
        const user = await await this.connection
            .createQueryBuilder(usersTable, usersTable)
            .orWhere(`${usersTable}.id = :id`, {
                id: userToken.userId,
            })
            .getOne();

        if (!user) throw new UnauthorizedException();

        return user;
    }

    /**
     * Login
     * @param data
     */
    async login(data: AuthDto) {
        // Encrypta password
        const { usersTable } = this.authConfigService.get();
        const { usersLoginColumn } = this.authConfigService.get();

        // Verificar se usuário existe
        const queryBuilder: any = this.connection.createQueryBuilder(usersTable, usersTable);

        // Busca login ou email
        usersLoginColumn.forEach(element => {
            queryBuilder.orWhere(`${usersTable}.${element} = :usersLoginColumn`, {
                usersLoginColumn: data.login,
            });
        });

        const user: any = await queryBuilder.getOne();

        // Usuário não encontrado ou senha inválida
        if (!user) {
            throw new UnauthorizedException('Dados incorretos');
        }

        // Senha inválida
        if (!(await Helpers.compareHashPassword(data.password, user.password))) {
            throw new UnauthorizedException('Dados incorretos');
        }

        // Gerar token
        const token = await this.generateToken(user.id);

        const payload = {
            id: user.id,
            name: user.name,
            token,
        };

        user.accessToken = this.jwtService.sign(payload);

        return user;
    }

    /**
     * Gera token para usuário
     * @param id
     */
    async generateToken(id: number | string): Promise<string> {
        const { userTokenTable } = this.authConfigService.get();

        const token = Helpers.generateUserToken();

        try {
            const userTokens = await this.connection
                .createQueryBuilder()
                .insert()
                .into(userTokenTable)
                .values([{ userId: id, token }])
                .execute();
        } catch (e) {
            // Se token já tiver sido utlizado, refaz função
            if (e.errno === 1062) {
                return this.generateToken(id);
            }
            throw e;
        }

        return token;
    }
}
