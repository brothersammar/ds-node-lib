import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { AuthDto } from './dto/auth-dto';

export abstract class AuthService {
    abstract async validateUser(token: string): Promise<any>;

    abstract async login(data: AuthDto);
}
