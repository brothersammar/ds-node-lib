import { Injectable, Inject, UnauthorizedException } from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose';
import { Connection, Collection } from 'mongoose';
import { JwtService } from '@nestjs/jwt';
import { Helpers } from '@devesharp/common';
import { AuthDto } from './dto/auth-dto';
import { AuthConfigService } from './config.service';
import { AuthService } from './auth.abstract.service';

@Injectable()
export class AuthMongooseService extends AuthService {
    constructor(
        @InjectConnection() private connection: Connection,
        private readonly authConfigService: AuthConfigService,
        private readonly jwtService: JwtService,
    ) {
        super();
    }

    /**
     * Valida token e retorna usuário
     * @param jwt
     */
    async validateUser(jwt: string): Promise<any> {
        const { userTokenTable } = this.authConfigService.get();
        const { usersTable } = this.authConfigService.get();

        // Payload
        const payload: any = this.jwtService.decode(jwt);
        if (!payload) return false;

        // Verificar token
        const userToken: any = await this.connection.collection(userTokenTable).findOne({
            token: payload.token,
        });

        if (!userToken) throw new UnauthorizedException();

        // Usuário
        const user = await this.connection.collection(usersTable).findOne({
            id: userToken.userId,
        });

        if (!user) throw new UnauthorizedException();

        return user;
    }

    /**
     * Login
     * @param data
     */
    async login(data: AuthDto) {
        // Encrypta password
        const { usersTable } = this.authConfigService.get();
        const { usersLoginColumn } = this.authConfigService.get();

        const queryBuilder: Collection = this.connection.collection('users');

        // Busca login ou email
        const query: any = [];

        usersLoginColumn.forEach(element => {
            query.push({
                [element]: data.login,
            });
        });

        const user: any = await queryBuilder.findOne({
            $or: query,
        });

        // Usuário não encontrado ou senha inválida
        if (!user) {
            throw new UnauthorizedException('Dados incorretos');
        }

        // Senha inválida
        if (!(await Helpers.compareHashPassword(data.password, user.password))) {
            throw new UnauthorizedException('Dados incorretos');
        }

        // Gerar token
        const token = await this.generateToken(user.id);

        const payload = {
            id: user.id,
            name: user.name,
            token,
        };

        user.accessToken = this.jwtService.sign(payload);

        return user;
    }

    /**
     * Gera token para usuário
     * @param id
     */
    async generateToken(id: number | string): Promise<string> {
        const { userTokenTable } = this.authConfigService.get();

        const token = Helpers.generateUserToken();

        try {
            const userTokens = await this.connection.collection(userTokenTable).insertOne({
                userId: id,
                token,
            });
        } catch (e) {
            if (e.code === 11000) {
                // Se token já tiver sido utilizado, refaz função
                return this.generateToken(id);
            }
            throw e;
        }

        return token;
    }
}
