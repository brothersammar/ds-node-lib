import { Test, TestingModule } from '@nestjs/testing';
import { JwtModule } from '@nestjs/jwt';
import { Helpers } from '@devesharp/common';
import { MongooseModule, getModelToken } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import { Mocks } from '../../../../__tests__/__mocks__/mocks';
import { UsersStubEntity } from './__mocks__/user-stub.entity';
import { UsersTokenStubEntity } from './__mocks__/user-token-stub.entity';
import { AuthController } from '../auth.controller';
import { AuthTypeOrmService } from '../auth-typeorm.service';
import { AuthConfigService } from '../config.service';
import { IAuthConfig } from '../interfaces';
import { AuthMongooseService } from '../auth-mongoose.service';
import { IUsersStub, UsersStubSchema } from './__mocks__/users-stub.schema';
import { IUsersTokensStub, UsersTokensStubSchema } from './__mocks__/users-tokens-stub.schema';

describe('Auth Controller TypeOrm', () => {
    let controller: AuthController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [AuthController],
            imports: [
                ...Mocks.databaseTypeOrm([UsersStubEntity, UsersTokenStubEntity]),
                JwtModule.register({
                    secret: 'SECRET_KEY',
                    signOptions: { expiresIn: '60s' },
                }),
            ],
            providers: [
                {
                    provide: 'CONFIG_OPTIONS',
                    useValue: {
                        secretKey: 'secret_key',
                        usersTable: 'users',
                        userTokenTable: 'users_tokens',
                        usersLoginColumn: ['login', 'email'],
                        usersPasswordColumn: 'password',
                    } as Partial<IAuthConfig>,
                },
                AuthConfigService,
                {
                    provide: 'AuthService',
                    useClass: AuthTypeOrmService,
                },
            ],
        }).compile();

        controller = module.get<AuthController>(AuthController);
    });

    it('deve ser definido', () => {
        expect(controller).toBeDefined();
    });

    it('deve realizar login', async () => {
        // Mock
        await UsersStubEntity.insert({
            login: 'john',
            email: 'john@gmail.com',
            password: await Helpers.getHashPassword('123456aa'),
        });

        const result = await controller.auth({
            login: 'john',
            password: '123456aa',
        });

        expect(typeof result.accessToken).toBe('string');
    });
});

describe('Auth Controller Mongoose', () => {
    let controller: AuthController;
    let userModel: Model<IUsersStub>;
    let userTokenModel: Model<IUsersTokensStub>;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [AuthController],
            imports: [
                ...Mocks.databaseMongoose(),
                JwtModule.register({
                    secret: 'SECRET_KEY',
                    signOptions: { expiresIn: '60s' },
                }),
                MongooseModule.forFeature([
                    { name: 'Users', schema: UsersStubSchema },
                    { name: 'Users_token', schema: UsersTokensStubSchema },
                ]),
            ],
            providers: [
                {
                    provide: 'CONFIG_OPTIONS',
                    useValue: {
                        secretKey: 'secret_key',
                        usersTable: 'users',
                        userTokenTable: 'users_tokens',
                        usersLoginColumn: ['login', 'email'],
                        usersPasswordColumn: 'password',
                    } as Partial<IAuthConfig>,
                },
                AuthConfigService,
                {
                    provide: 'AuthService',
                    useClass: AuthMongooseService,
                },
            ],
        }).compile();

        controller = module.get<AuthController>(AuthController);

        userModel = module.get<Model<IUsersStub>>(getModelToken('Users'));
        userTokenModel = module.get<Model<IUsersTokensStub>>(getModelToken('Users_token'));

        await userModel.deleteMany({});
        await userTokenModel.deleteMany({});
        await userTokenModel.syncIndexes({});
    });

    it('deve ser definido', () => {
        expect(controller).toBeDefined();
    });

    it('deve realizar login', async () => {
        // Mock
        await userModel.insertMany([
            {
                login: 'john',
                email: 'john@gmail.com',
                password: await Helpers.getHashPassword('123456aa-mongo'),
            },
        ]);
        const result = await controller.auth({
            login: 'john',
            password: '123456aa-mongo',
        });

        expect(typeof result.accessToken).toBe('string');
    });
});
