import { Test, TestingModule } from '@nestjs/testing';
import { JwtModule } from '@nestjs/jwt';
import { Helpers } from '@devesharp/common';
import { UnauthorizedException } from '@nestjs/common';
import { AuthTypeOrmService } from '../auth-typeorm.service';
import { AuthConfigService } from '../config.service';
import { IAuthConfig } from '../interfaces';
import { Mocks } from '../../../../__tests__/__mocks__/mocks';
import { UsersStubEntity } from './__mocks__/user-stub.entity';
import { UsersTokenStubEntity } from './__mocks__/user-token-stub.entity';

describe('AuthService', () => {
    let service: AuthTypeOrmService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ...Mocks.databaseTypeOrm([UsersStubEntity, UsersTokenStubEntity]),
                JwtModule.register({
                    secret: 'SECRET_KEY',
                    signOptions: { expiresIn: '60s' },
                }),
            ],
            providers: [
                AuthTypeOrmService,
                {
                    provide: 'CONFIG_OPTIONS',
                    useValue: {
                        secretKey: 'secret_key',
                        usersTable: 'users',
                        userTokenTable: 'users_tokens',
                        usersLoginColumn: ['login', 'email'],
                        usersPasswordColumn: 'password',
                    } as Partial<IAuthConfig>,
                },
                AuthConfigService,
            ],
        }).compile();

        service = module.get<AuthTypeOrmService>(AuthTypeOrmService);

        // Limpar banco de dados
        await UsersStubEntity.clear();
        await UsersTokenStubEntity.clear();
    });

    it('deve ser definido', () => {
        expect(service).toBeDefined();
    });

    it('deve retornar token quando passado login e senha corretos', async () => {
        // Mock
        await UsersStubEntity.insert({
            login: 'john',
            email: 'john@gmail.com',
            password: await Helpers.getHashPassword('123456aa'),
        });

        // Verifica login
        const token = await service.login({
            login: 'john',
            password: '123456aa',
        });

        expect(typeof token.accessToken).toBe('string');
    });

    it('deve retornar erro não autorizado quando passado login e senha incorretos', async () => {
        const login = service.login({
            login: 'john',
            password: '123456aa',
        });

        await expect(login).rejects.toThrow(UnauthorizedException);
        await expect(login).rejects.toThrow('Dados incorretos');
    });

    it('deve retornar erro não autorizado quando passada senha incorreta', async () => {
        // Mock
        await UsersStubEntity.insert({
            login: 'john',
            email: 'john@gmail.com',
            password: await Helpers.getHashPassword('123456aa'),
        });

        //
        const login = service.login({
            login: 'john',
            password: '123456bb',
        });

        await expect(login).rejects.toThrow(UnauthorizedException);
        await expect(login).rejects.toThrow('Dados incorretos');
    });

    it('não deve utilizar mesmo token para login', async () => {
        expect.assertions(1);

        // Cria um token igual até a 4 chamada da função
        let call = 0;
        Helpers.generateUserToken = () => {
            call += 1;
            if (call === 4) {
                expect(1).toBe(1);
                return 'other-token';
            }
            return 'same-token';
        };

        await service.generateToken(1);
        await service.generateToken(1);
    });

    it('deve retornar usuário caso token esteja correto', async () => {
        // Mock
        await UsersStubEntity.insert({
            login: 'john',
            email: 'john@gmail.com',
            password: await Helpers.getHashPassword('123456aa'),
        });
        const user = await service.login({
            login: 'john',
            password: '123456aa',
        });

        //
        const userLogged = await service.validateUser(user.accessToken);

        expect(userLogged.login).toBe('john');
        expect(userLogged.email).toBe('john@gmail.com');
    });
});
