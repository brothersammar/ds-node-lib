import { Test, TestingModule } from '@nestjs/testing';
import { AuthModule } from '../auth.module';
import { Mocks } from '../../../../__tests__/__mocks__/mocks';

describe('Auth Controller TypeOrm', () => {
    let databaseModule: AuthModule;

    it('deve conseguir definir AuthModule.forMongoose', async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ...Mocks.databaseMongoose(),
                AuthModule.forMongoose({
                    secretKey: 'secret_key',
                    usersTable: 'users',
                    userTokenTable: 'users_tokens',
                    usersLoginColumn: ['login', 'email'],
                    usersPasswordColumn: 'password',
                }),
            ],
        }).compile();

        databaseModule = module.get<AuthModule>(AuthModule);

        expect(databaseModule).toBeDefined();
    });

    it('deve conseguir definir AuthModule.forTypeORM', async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ...Mocks.databaseTypeOrm(),
                AuthModule.forTypeORM({
                    secretKey: 'secret_key',
                    usersTable: 'users',
                    userTokenTable: 'users_tokens',
                    usersLoginColumn: ['login', 'email'],
                    usersPasswordColumn: 'password',
                }),
            ],
        }).compile();

        databaseModule = module.get<AuthModule>(AuthModule);

        expect(databaseModule).toBeDefined();
    });
});
