import { Test, TestingModule } from '@nestjs/testing';
import { JwtModule } from '@nestjs/jwt';
import { Helpers } from '@devesharp/common';
import { UnauthorizedException } from '@nestjs/common';
import { MongooseModule, getModelToken } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import { AuthMongooseService } from '../auth-mongoose.service';
import { AuthConfigService } from '../config.service';
import { IAuthConfig } from '../interfaces';
import { Mocks } from '../../../../__tests__/__mocks__/mocks';
import { UsersStubSchema, IUsersStub } from './__mocks__/users-stub.schema';
import { UsersTokensStubSchema, IUsersTokensStub } from './__mocks__/users-tokens-stub.schema';

describe('AuthService', () => {
    let service: AuthMongooseService;
    let userModel: Model<IUsersStub>;
    let userTokenModel: Model<IUsersTokensStub>;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                ...Mocks.databaseMongoose(),
                JwtModule.register({
                    secret: 'SECRET_KEY',
                    signOptions: { expiresIn: '60s' },
                }),
                MongooseModule.forFeature([
                    { name: 'Users', schema: UsersStubSchema },
                    { name: 'Users_token', schema: UsersTokensStubSchema },
                ]),
            ],
            providers: [
                AuthMongooseService,
                {
                    provide: 'CONFIG_OPTIONS',
                    useValue: {
                        secretKey: 'secret_key',
                        usersTable: 'users',
                        userTokenTable: 'users_tokens',
                        usersLoginColumn: ['login', 'email'],
                        usersPasswordColumn: 'password',
                    } as Partial<IAuthConfig>,
                },
                AuthConfigService,
            ],
        }).compile();

        service = module.get<AuthMongooseService>(AuthMongooseService);

        userModel = module.get<Model<IUsersStub>>(getModelToken('Users'));
        userTokenModel = module.get<Model<IUsersTokensStub>>(getModelToken('Users_token'));

        await userModel.deleteMany({});
        await userTokenModel.deleteMany({});
        await userTokenModel.syncIndexes({});
    });

    it('deve ser definido', () => {
        expect(service).toBeDefined();
    });

    it('deve retornar token quando passado login e senha corretos', async () => {
        await userModel.insertMany([
            {
                login: 'john',
                email: 'john@gmail.com',
                password: await Helpers.getHashPassword('123456aa'),
            },
        ]);

        // Verifica login
        const token = await service.login({
            login: 'john',
            password: '123456aa',
        });

        expect(typeof token.accessToken).toBe('string');
    });

    it('deve retornar erro não autorizado quando passado login e senha incorretos', async () => {
        const login = service.login({
            login: 'john',
            password: '123456aa',
        });

        await expect(login).rejects.toThrow(UnauthorizedException);
        await expect(login).rejects.toThrow('Dados incorretos');
    });

    it('deve retornar erro não autorizado quando passada senha incorreta', async () => {
        // Mock
        await userModel.insertMany([
            {
                login: 'john',
                email: 'john@gmail.com',
                password: await Helpers.getHashPassword('123456aa'),
            },
        ]);

        //
        const login = service.login({
            login: 'john',
            password: '123456bb',
        });

        await expect(login).rejects.toThrow(UnauthorizedException);
        await expect(login).rejects.toThrow('Dados incorretos');
    });

    it('não deve utilizar mesmo token para login', async () => {
        expect.assertions(1);

        // Cria um token igual até a 4 chamada da função
        let call = 0;
        Helpers.generateUserToken = () => {
            call += 1;
            if (call === 4) {
                expect(1).toBe(1);
                return 'other-token';
            }
            return 'same-token';
        };

        await service.generateToken(1);
        await service.generateToken(1);
    });

    it('deve retornar usuário caso token esteja correto', async () => {
        // Mock
        await userModel.insertMany([
            {
                login: 'john',
                email: 'john@gmail.com',
                password: await Helpers.getHashPassword('123456aa'),
            },
        ]);
        const user = await service.login({
            login: 'john',
            password: '123456aa',
        });

        //
        const userLogged = await service.validateUser(user.accessToken);

        expect(userLogged.login).toBe('john');
        expect(userLogged.email).toBe('john@gmail.com');
    });
});
