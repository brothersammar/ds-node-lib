import { Controller, Get, UseGuards, Request } from '@nestjs/common';
import { AuthGuard } from '../../auth.guard';

@Controller('foo')
@UseGuards(AuthGuard)
export class ControllerStub {
    @Get()
    foo(@Request() res: any): any {
        return {
            id: res.user.id,
            email: res.user.email,
            login: res.user.login,
        };
    }
}
