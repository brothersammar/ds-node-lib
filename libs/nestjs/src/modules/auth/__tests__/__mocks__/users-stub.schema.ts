import * as mongoose from 'mongoose';

export interface IUsersStub extends mongoose.Document {
    login: string;
    email: string;
    password: string;
}

export const UsersStubSchema: mongoose.Schema = new mongoose.Schema({
    login: { type: String },
    email: { type: String },
    password: { type: String },
});
