import * as mongoose from 'mongoose';

export interface IUsersTokensStub extends mongoose.Document {
    userId: string;
    token: string;
}

export const UsersTokensStubSchema: mongoose.Schema = new mongoose.Schema({
    userId: { type: String },
    token: { type: String, unique: true },
});
