import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { APP_GUARD } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { Helpers } from '@devesharp/common';
import { MongooseModule, getModelToken } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import { AuthGuard } from '../auth.guard';
import { Mocks } from '../../../../__tests__/__mocks__/mocks';
import { UsersStubEntity } from './__mocks__/user-stub.entity';
import { UsersTokenStubEntity } from './__mocks__/user-token-stub.entity';
import { ControllerStub } from './__mocks__/controller.mock';
import { AuthController } from '../auth.controller';
import { AuthTypeOrmService } from '../auth-typeorm.service';
import { AuthConfigService } from '../config.service';
import { IAuthConfig } from '../interfaces';
import { AuthService } from '../auth.abstract.service';
import { IUsersStub, UsersStubSchema } from './__mocks__/users-stub.schema';
import { IUsersTokensStub, UsersTokensStubSchema } from './__mocks__/users-tokens-stub.schema';
import { AuthMongooseService } from '../auth-mongoose.service';

describe('AuthGuard TypeOrm', () => {
    let service: AuthService;
    let module: TestingModule;

    beforeEach(async () => {
        module = await Test.createTestingModule({
            controllers: [ControllerStub, AuthController],
            imports: [
                ...Mocks.databaseTypeOrm([UsersStubEntity, UsersTokenStubEntity]),
                JwtModule.register({
                    secret: 'SECRET_KEY',
                    signOptions: { expiresIn: '60s' },
                }),
            ],
            providers: [
                {
                    provide: 'CONFIG_OPTIONS',
                    useValue: {
                        secretKey: 'secret_key',
                        usersTable: 'users',
                        userTokenTable: 'users_tokens',
                        usersLoginColumn: ['login', 'email'],
                        usersPasswordColumn: 'password',
                    } as Partial<IAuthConfig>,
                },
                AuthConfigService,
                {
                    provide: 'AuthService',
                    useClass: AuthTypeOrmService,
                },
                {
                    provide: APP_GUARD,
                    useValue: AuthGuard,
                },
            ],
        }).compile();

        service = module.get<AuthService>(AuthService);

        // Limpar banco de dados
        await UsersStubEntity.clear();
        await UsersTokenStubEntity.clear();
    });

    it('deve bloquear acesso (não autorizado)', async () => {
        const app = await module.createNestApplication();
        await app.init();
        return request(app.getHttpServer())
            .get('/foo')
            .expect(401);
    });

    it('deve bloquear acesso (token inválido)', async () => {
        const app = await module.createNestApplication();
        await app.init();
        return request(app.getHttpServer())
            .get('/foo')
            .set('Authrization', 'invalid-token')
            .expect(401);
    });

    it('deve permitir acesso a rota com token valido', async () => {
        // Mock
        await UsersStubEntity.insert({
            login: 'john',
            email: 'john@gmail.com',
            password: await Helpers.getHashPassword('123456aa'),
        });

        // Verifica login
        const token = await service.login({
            login: 'john',
            password: '123456aa',
        });

        const app = await module.createNestApplication();
        await app.init();

        return request(app.getHttpServer())
            .get('/foo')
            .set('Authorization', `Bearer ${token.accessToken}`)
            .expect(200)
            .expect({
                id: 1,
                login: 'john',
                email: 'john@gmail.com',
            });
    });
});

describe('AuthGuard Mongoose', () => {
    let service: AuthService;
    let module: TestingModule;
    let userModel: Model<IUsersStub>;
    let userTokenModel: Model<IUsersTokensStub>;

    beforeEach(async () => {
        module = await Test.createTestingModule({
            controllers: [ControllerStub, AuthController],
            imports: [
                ...Mocks.databaseMongoose(),
                JwtModule.register({
                    secret: 'SECRET_KEY',
                    signOptions: { expiresIn: '60s' },
                }),
                MongooseModule.forFeature([
                    { name: 'Users', schema: UsersStubSchema },
                    { name: 'Users_token', schema: UsersTokensStubSchema },
                ]),
            ],
            providers: [
                {
                    provide: 'CONFIG_OPTIONS',
                    useValue: {
                        secretKey: 'secret_key',
                        usersTable: 'users',
                        userTokenTable: 'users_tokens',
                        usersLoginColumn: ['login', 'email'],
                        usersPasswordColumn: 'password',
                    } as Partial<IAuthConfig>,
                },
                AuthConfigService,
                {
                    provide: 'AuthService',
                    useClass: AuthMongooseService,
                },
                {
                    provide: APP_GUARD,
                    useValue: AuthGuard,
                },
            ],
        }).compile();

        service = module.get<AuthService>(AuthService);

        userModel = module.get<Model<IUsersStub>>(getModelToken('Users'));
        userTokenModel = module.get<Model<IUsersTokensStub>>(getModelToken('Users_token'));

        await userModel.deleteMany({});
        await userTokenModel.deleteMany({});
        await userTokenModel.syncIndexes({});
    });

    it('deve bloquear acesso (não autorizado)', async () => {
        const app = await module.createNestApplication();
        await app.init();
        return request(app.getHttpServer())
            .get('/foo')
            .expect(401);
    });

    it('deve bloquear acesso (token inválido)', async () => {
        const app = await module.createNestApplication();
        await app.init();
        return request(app.getHttpServer())
            .get('/foo')
            .set('Authrization', 'invalid-token')
            .expect(401);
    });

    it('deve permitir acesso a rota com token valido', async () => {
        // Mock
        const user = await userModel.create({
            login: 'john',
            email: 'john@gmail.com',
            password: await Helpers.getHashPassword('123456aa-mongo'),
        });

        // Verifica login
        const token = await service.login({
            login: 'john',
            password: '123456aa-mongo',
        });

        const app = await module.createNestApplication();
        await app.init();

        return request(app.getHttpServer())
            .get('/foo')
            .set('Authorization', `Bearer ${token.accessToken}`)
            .expect(200);
    });
});
