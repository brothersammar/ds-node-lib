import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { isObject } from 'util';

export class AuthDto {
    @IsNotEmpty()
    @IsString()
    public login: string;

    @IsNotEmpty()
    @IsString()
    public password: string;
}
