import { Controller, Post, Body, Inject } from '@nestjs/common';
import { AuthDto } from './dto/auth-dto';
import { AuthService } from './auth.abstract.service';

@Controller('auth')
export class AuthController {
    constructor(@Inject('AuthService') private authService: AuthService) {}

    @Post()
    auth(@Body() body: AuthDto) {
        return this.authService.login(body);
    }
}
