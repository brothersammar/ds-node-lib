import { Module, Global, DynamicModule } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthTypeOrmService } from './auth-typeorm.service';
// import { UsersModule } from '../users/users.module';
import { AuthController } from './auth.controller';
import { IAuthConfig } from './interfaces';
import { AuthConfigService } from './config.service';
import { AuthMongooseService } from './auth-mongoose.service';

@Global()
@Module({})
export class AuthModule {
    /**
     * Modulo para autentificação com mongoose
     * @param options
     */
    static forMongoose(options: IAuthConfig): DynamicModule {
        return {
            module: AuthModule,
            imports: [
                JwtModule.register({
                    secret: 'SECRET_KEY',
                    signOptions: { expiresIn: '360d' },
                }),
            ],
            providers: [
                {
                    provide: 'AuthService',
                    useClass: AuthMongooseService,
                },
                {
                    provide: 'CONFIG_OPTIONS',
                    useValue: options,
                },
                AuthConfigService,
            ],
            exports: [],
            controllers: [AuthController],
        };
    }

    /**
     * Modulo para autentificação com typeorm
     * @param options
     */
    static forTypeORM(options: IAuthConfig): DynamicModule {
        return {
            module: AuthModule,
            imports: [
                JwtModule.register({
                    secret: 'SECRET_KEY',
                    signOptions: { expiresIn: '360d' },
                }),
            ],
            providers: [
                {
                    provide: 'AuthService',
                    useClass: AuthTypeOrmService,
                },
                {
                    provide: 'CONFIG_OPTIONS',
                    useValue: options,
                },
                AuthConfigService,
            ],
            exports: [],
            controllers: [AuthController],
        };
    }
}
