import { CreateDateColumn, UpdateDateColumn, Entity, PrimaryGeneratedColumn, Column, BaseEntity } from 'typeorm';

@Entity()
export class UsersEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ default: true })
    enabled: boolean;

    @Column()
    name: string;

    @Column()
    password: string;

    @Column()
    email: string;

    @Column()
    login: string;

    @Column()
    role: number;

    @Column({ nullable: true })
    lastAccess: Date;

    @CreateDateColumn()
    createAt: string;

    @UpdateDateColumn()
    updateAt: string;

    @Column({ nullable: true })
    deleteAt: Date;
}
