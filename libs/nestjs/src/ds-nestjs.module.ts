import { Module } from '@nestjs/common';
import { DSNestJSService } from './ds-nestjs.service';
import { AuthModule } from './modules/auth/auth.module';
import { DatabaseModule } from './modules/database/database.module';

@Module({
	providers: [DSNestJSService],
	exports: [DSNestJSService],
	imports: [AuthModule, DatabaseModule],
})
export class DSNestJSModule {}
