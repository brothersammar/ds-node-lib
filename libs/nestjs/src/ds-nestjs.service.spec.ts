import { Test, TestingModule } from '@nestjs/testing';
import { DSNestJSService } from './ds-nestjs.service';

describe('DSNestJSService', () => {
    let service: DSNestJSService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [DSNestJSService],
        }).compile();

        service = module.get<DSNestJSService>(DSNestJSService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
