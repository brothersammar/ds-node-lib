import * as dotenv from 'dotenv';
import * as Joi from '@hapi/joi';

export class ConfigSetup<T> {
    private file: string;

    private envConfig: T;

    constructor(schema: Joi.ObjectSchema, dir?: string) {
        let dirname = dir;
        if (!dirname) {
            dirname = ``;
        }

        this.file = `${process.env.NODE_ENV || ''}.env`;
        const config = dotenv.config({
            path: dirname + this.file,
        });

        if (config.error) {
            throw new Error(config.error.toString());
        }

        this.envConfig = this.validate(config.parsed, schema);
    }

    /**
     *Valida o env que estiver ativo
     *
     * @param {EnvConfig} envConfig
     * @param {Joi.ObjectSchema} schema
     * @returns {EnvConfig}
     * @memberof ConfigService
     */
    validate(envConfig: any, schema: Joi.ObjectSchema): T {
        const envVarsSchema: Joi.ObjectSchema = schema;

        const { error, value: validatedEnvConfig } = envVarsSchema.validate(envConfig);

        if (error) {
            throw new Error(`${this.file} validation error: ${error.message}`);
        }
        return validatedEnvConfig;
    }

    getConfig(): T {
        return this.envConfig;
    }
}
