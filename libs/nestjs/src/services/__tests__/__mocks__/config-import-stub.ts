/**
 * Exemplo de uso do ConfigSetup
 */
import * as Joi from '@hapi/joi';
import { ConfigSetup } from '../../config.service';

interface IConfig {
    NODE_ENV: 'development' | 'production' | 'test' | 'provision';
    PORT: number;
    API_AUTH_ENABLED: boolean;
}

const schema: Joi.ObjectSchema = Joi.object({
    NODE_ENV: Joi.string()
        .valid('development', 'production', 'test', 'provision')
        .default('development'),
    PORT: Joi.number().default(3000),
    API_AUTH_ENABLED: Joi.boolean().required(),
});

process.env.NODE_ENV = 'mock';

export default new ConfigSetup<IConfig>(schema, `${__dirname}/`).getConfig();
