import { BaseEntity, Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('users')
export class UsersStub extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id?: number;

    @Column({ default: 1 })
    enabled?: number;

    @Column()
    name?: string;

    @Column({ default: '123456aa' })
    password?: string;

    @Column({ default: 1 })
    age: number;

    @Column({ default: 10 })
    bloodType: number;

    @Column({ type: 'datetime', nullable: true })
    deletedAt?: Date;
}
