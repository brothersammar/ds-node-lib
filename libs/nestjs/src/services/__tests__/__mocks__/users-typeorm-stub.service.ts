import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TypeOrmCrudManagerService } from '../../typeorm-crud-manager.service';
import { UsersStub } from './users-stub.entity';

@Injectable()
export class UsersService extends TypeOrmCrudManagerService<UsersStub, any> {
    constructor(@InjectRepository(UsersStub) private readonly userRepository: Repository<UsersStub>) {
        super(userRepository);
    }
}
