import {
    BaseEntity,
    Column,
    Connection,
    getConnection,
    createConnection,
    Entity,
    EntityManager,
    PrimaryGeneratedColumn,
    SelectQueryBuilder,
} from 'typeorm';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { format } from 'date-fns';
import { TypeOrmCrudManagerService } from '../typeorm-crud-manager.service';
import { IBodySearch } from '../../interfaces/crud-interfaces.interface';
import { Mocks } from '../../../__tests__/__mocks__/mocks';
import { UsersStub } from './__mocks__/users-stub.entity';
import { UsersService } from './__mocks__/users-typeorm-stub.service';

describe('TypeOrmCrudManagerService', () => {
    let service: UsersService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [...Mocks.databaseTypeOrm([UsersStub]), TypeOrmModule.forFeature([UsersStub])],
            providers: [UsersService],
        }).compile();

        await UsersStub.clear();

        service = module.get<UsersService>(UsersService);
    });

    it('offset e filters', async () => {
        const filters = {
            limit: 3,
            offset: 4,
        };

        /**
         * Teste da funcao
         */
        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        //
        const query = service.__query(queryBuilder, filters);
        expect(/(.*) LIMIT 3 OFFSET 4/.test(query.getQuery())).toBe(true);
    }, 15000);

    it('only offset', async () => {
        const filters = {
            offset: 1,
        };

        /**
         * Teste da funcao
         */
        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        //
        const query = service.__query(queryBuilder, filters);
        expect(/(.*) OFFSET 1/.test(query.getQuery())).toBe(true);
    }, 15000);

    it('only limit', async () => {
        const filters = {
            limit: 4,
        };

        /**
         * Teste da funcao
         */
        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        //
        const query = service.__query(queryBuilder, filters);
        expect(/(.*) LIMIT 4/.test(query.getQuery())).toBe(true);
    }, 15000);

    it('Limit não pode exceder limitMax', async () => {
        // Limite máximo do valor limite
        service.limitMax = 10;

        const filters = {
            limit: 15,
        };

        /**
         * Teste da funcao
         */
        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        //
        const query = service.__query(queryBuilder, filters);
        expect(/(.*) LIMIT 10/.test(query.getQuery())).toBe(true);
    }, 15000);

    it('sem nenhum valor', async () => {
        const filters = {};

        /**
         * Teste da funcao
         */
        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        //
        const query = service.__query(queryBuilder, filters);
        expect(/(.*) LIMIT 20/.test(query.getQuery())).toBe(true);
    }, 15000);

    it('um valor inválido', async () => {
        const filters = {
            limit: -100,
            offset: -50,
        };

        /**
         * Teste da funcao
         */
        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        //
        const query = service.__query(queryBuilder, filters);
        expect(/(.*) LIMIT 20/.test(query.getQuery())).toBe(true);
    }, 15000);

    it('um valor invalido - não número', async () => {
        const filters = {
            limit: 'sdsd',
            offset: 'sdsd',
        };

        /**
         * Teste da funcao
         */
        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        //
        const query = service.__query(queryBuilder, filters);
        expect(/(.*) LIMIT 20/.test(query.getQuery())).toBe(true);
    }, 15000);

    it('should be change offset with page 1', async () => {
        const filters = {
            page: 1,
        };

        /**
         * Teste da funcao
         */
        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        //
        const query = service.__query(queryBuilder, filters);
        expect(/(.*) LIMIT 20/.test(query.getQuery())).toBe(true);
    }, 15000);

    it('should be change offset with page 1', async () => {
        const filters = {
            page: 3,
        };

        /**
         * Teste da funcao
         */
        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        //
        const query = service.__query(queryBuilder, filters);
        expect(/(.*) LIMIT 20 OFFSET 40/.test(query.getQuery())).toBe(true);
    }, 15000);
});
