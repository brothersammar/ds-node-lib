import {
    BaseEntity,
    Column,
    Connection,
    getConnection,
    createConnection,
    Entity,
    EntityManager,
    PrimaryGeneratedColumn,
    SelectQueryBuilder,
} from 'typeorm';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmCrudManagerService } from '../typeorm-crud-manager.service';
import { IBodySearch } from '../../interfaces/crud-interfaces.interface';

import { Mocks } from '../../../__tests__/__mocks__/mocks';
import { UsersStub } from './__mocks__/users-stub.entity';
import { UsersService } from './__mocks__/users-typeorm-stub.service';

// @Entity()
// export class UsersStub extends BaseEntity {
//     @PrimaryGeneratedColumn()
//     id: number;

//     @Column({ default: 1 })
//     enabled?: number;

//     @Column()
//     name: string;

//     @Column()
//     age: number;

//     @Column({ default: 10 })
//     bloodType: number;
// }

describe('TypeOrmCrudManagerService', () => {
    let service: UsersService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [...Mocks.databaseTypeOrm([UsersStub]), TypeOrmModule.forFeature([UsersStub])],
            providers: [UsersService],
        }).compile();

        await UsersStub.clear();

        service = module.get<UsersService>(UsersService);
    });

    it('Projection não deve funcionar se não for enviado projection custom estiver vazio', () => {
        // Valores obrigatórios do projection
        service.aliases = ['name', 'age', 'id'];
        // Projections custom
        const aliases = [];

        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        //
        const results = service.__projection(queryBuilder, aliases);
        results.select(['*']);

        // Quey padrão do TypeOrm
        expect(results.getQuery()).toBe('SELECT * FROM `users` `UsersStub`');
    });

    it('Projection deve retornar apenas valores enviados por parametros', async () => {
        await UsersStub.insert([{ name: 'A', age: 3 }]);
        await UsersStub.insert([{ name: 'B', age: 2 }]);
        await UsersStub.insert([{ name: 'C', age: 1 }]);
        await UsersStub.insert([{ name: 'D', age: 0 }]);

        const aliases = ['id', 'name'];

        //
        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        const results = service.__projection(queryBuilder, aliases);

        expect(results.getQuery()).toBe('SELECT `UsersStub`.`id`, `UsersStub`.`name` FROM `users` `UsersStub`');

        const items = await results.getRawMany();

        expect(items[0]).toEqual({
            id: '1',
            name: 'A',
        });
    }, 15000);

    it('Projection deve retornar paramatros custom + obrigatórios', async () => {
        await UsersStub.insert([{ name: 'A', age: 3 }]);

        service.aliases = ['name', 'age'];
        const aliases = ['id', 'enabled'];

        //
        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        const results = await service.__projection(queryBuilder, aliases).getRawMany();

        /**
         * Retonar apenas os valores de
         * age, bloodType
         * + name
         * */
        expect(results[0]).toEqual({
            id: '1',
            enabled: 1,
            name: 'A',
            age: 3,
        });
    }, 15000);

    it('Projection deve ignorar valores inválidos', async () => {
        await UsersStub.insert([{ name: 'A', age: 3 }]);

        service.aliases = ['name', 'id'];
        service.aliasesAllowed = ['name'];
        const aliases: any = [1000, 'school', 'name'];

        /**
         *  Executa o teste
         * */
        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        const results = await service.__projection(queryBuilder, aliases).getRawMany();

        /**
         * Retonar apenas os valores de
         * + name, id
         * */
        expect(results[0]).toEqual({
            id: '1',
            name: 'A',
        });
    });
});
