import { MongooseCrudManager } from '../mongoose-crud-manager.service';
import * as mongoose from 'mongoose';
import { format } from 'date-fns';
import { IBodySearch } from '../../interfaces/crud-interfaces.interface';

export interface IUser extends mongoose.Document {
	enabled: boolean;
	name: string;
	password: string;
	age: number;
	deleted_at: string;
}

const UserSchema: mongoose.Schema = new mongoose.Schema({
	enabled: { type: Boolean },
	name: { type: String },
	password: { type: String },
	age: { type: Number },
	deleted_at: { type: String },
});

// Export the model and return your IUser interface
let Users = mongoose.model<IUser>('User', UserSchema);

let service: MongooseCrudManager<IUser>;

beforeEach(async () => {
	service = new MongooseCrudManager<IUser>(
		Users,
		// new EntityManager(null as Connection),
	);
	// service.
	await Users.deleteMany({});
});

beforeAll(async () => {
	await mongoose.connect('mongodb://admin:123456aa@localhost:9312/testing?authSource=admin', {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});
});

describe('MongooseCrudManager', () => {
	it('should be return select builder with filter int', async () => {
		await service.create({ age: 1 });
		await service.create({ age: 2 });

		let queryBuilder = service.model.find();

		service.filters = {
			int: {
				column: 'age',
				filter: 'int',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			int: 2,
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({ age: 2 });

		let results = await queryBuilder;
		expect(1).toEqual(results.length);
		expect(2).toEqual(results[0].age);
	});

	it('should be return select builder with filter int_gt', async () => {
		await service.create({ age: 1 });
		await service.create({ age: 2 });
		await service.create({ age: 3 });

		let queryBuilder = service.model.find();

		service.filters = {
			int_gt: {
				column: 'age',
				filter: 'int_gt',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			int_gt: 1,
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({ age: { $gt: 1 } });

		let results = await queryBuilder;
		expect(2).toEqual(results.length);
		expect(2).toEqual(results[0].age);
		expect(3).toEqual(results[1].age);
	});

	it('should be return select builder with filter int_lt', async () => {
		await service.create({ age: 1 });
		await service.create({ age: 2 });
		await service.create({ age: 3 });

		let queryBuilder = service.model.find();

		service.filters = {
			int_lt: {
				column: 'age',
				filter: 'int_lt',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			int_lt: 3,
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({ age: { $lt: 3 } });

		let results = await queryBuilder;
		expect(2).toEqual(results.length);
		expect(1).toEqual(results[0].age);
		expect(2).toEqual(results[1].age);
	});

	it('should be return select builder with filter int_gte', async () => {
		await service.create({ age: 1 });
		await service.create({ age: 2 });
		await service.create({ age: 3 });

		let queryBuilder = service.model.find();

		service.filters = {
			int_gte: {
				column: 'age',
				filter: 'int_gte',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			int_gte: 2,
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({ age: { $gte: 2 } });

		let results = await queryBuilder;
		expect(2).toEqual(results.length);
		expect(2).toEqual(results[0].age);
		expect(3).toEqual(results[1].age);
	});

	it('should be return select builder with filter int_lte', async () => {
		await service.create({ age: 1 });
		await service.create({ age: 2 });
		await service.create({ age: 3 });

		let queryBuilder = service.model.find();

		service.filters = {
			int_lte: {
				column: 'age',
				filter: 'int_lte',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			int_lte: 2,
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({ age: { $lte: 2 } });

		let results = await queryBuilder;
		expect(2).toEqual(results.length);
		expect(1).toEqual(results[0].age);
		expect(2).toEqual(results[1].age);
	});

	it('should be return select builder with filter string', async () => {
		await service.create({ name: 'Veronica' });
		await service.create({ name: 'Veronica 2' });
		await service.create({ name: 'John' });

		let queryBuilder = service.model.find();

		service.filters = {
			string: {
				column: 'name',
				filter: 'string',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			string: 'Veronica',
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({ name: 'Veronica' });

		let results = await queryBuilder;
		expect(results.length).toEqual(1);
		expect(results[0].name).toEqual('Veronica');
	});

	it('should be return select builder with filter string_like', async () => {
		await service.create({ name: 'VeRonIca' });
		await service.create({ name: 'VeRonIca 2' });
		await service.create({ name: 'John' });

		let queryBuilder = service.model.find();

		service.filters = {
			string: {
				column: 'name',
				filter: 'string_like',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			string: 'veronica',
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({
			name: { $regex: '^veronica$', $options: 'i' },
		});

		let results = await queryBuilder;
		expect(results.length).toEqual(1);
		expect(results[0].name).toEqual('VeRonIca');
	});

	it('should be return select builder with filter string_like_not', async () => {
		await service.create({ name: 'VeRonIca' });
		await service.create({ name: 'John' });

		let queryBuilder = service.model.find();

		service.filters = {
			string_like_not: {
				column: 'name',
				filter: 'string_like_not',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			string_like_not: 'veronica',
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({
			name: { $regex: '^((?!veronica).)*$', $options: 'i' },
		});

		let results = await queryBuilder;
		expect(results.length).toEqual(1);
		expect(results[0].name).toEqual('John');
	});

	it('should be return select builder with filter array_int', async () => {
		await service.create({ age: 1 });
		await service.create({ age: 2 });
		await service.create({ age: 3 });

		let queryBuilder = service.model.find();

		service.filters = {
			array_int: {
				column: 'age',
				filter: 'array_int',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			array_int: [1, 3],
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({ age: { $in: [1, 3] } });

		let results = await queryBuilder;
		expect(2).toEqual(results.length);
		expect(1).toEqual(results[0].age);
		expect(3).toEqual(results[1].age);
	});

	it('should be return select builder with filter array_int_not', async () => {
		await service.create({ age: 1 });
		await service.create({ age: 2 });
		await service.create({ age: 3 });

		let queryBuilder = service.model.find();

		service.filters = {
			array_int_not: {
				column: 'age',
				filter: 'array_int_not',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			array_int_not: [1, 3],
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({
			age: { $nin: [1, 3] },
		});

		let results = await queryBuilder;
		expect(1).toEqual(results.length);
		expect(2).toEqual(results[0].age);
	});

	it('should be return select builder with filter array_int_not', async () => {
		await service.create({ age: 1 });
		await service.create({ age: 2 });
		await service.create({ age: 3 });

		let queryBuilder = service.model.find();

		service.filters = {
			array_int_not: {
				column: 'age',
				filter: 'array_int_not',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			array_int_not: [1, 3],
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({
			age: { $nin: [1, 3] },
		});

		let results = await queryBuilder;
		expect(1).toEqual(results.length);
		expect(2).toEqual(results[0].age);
	});

	it('should be return select builder with filter array_string', async () => {
		await service.create({ name: 'Veronica' });
		await service.create({ name: 'John' });
		await service.create({ name: 'Jude' });

		let queryBuilder = service.model.find();

		service.filters = {
			array_string: {
				column: 'name',
				filter: 'array_string',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			array_string: ['Veronica', 'Jude'],
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({
			name: { $in: ['Veronica', 'Jude'] },
		});

		let results = await queryBuilder;
		expect(results.length).toEqual(2);
		expect(results[0].name).toEqual('Veronica');
		expect(results[1].name).toEqual('Jude');
	});

	it('should be return select builder with filter array_string_not', async () => {
		await service.create({ name: 'Veronica' });
		await service.create({ name: 'John' });
		await service.create({ name: 'Jude' });

		let queryBuilder = service.model.find();

		service.filters = {
			array_string_not: {
				column: 'name',
				filter: 'array_string_not',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			array_string_not: ['Veronica', 'Jude'],
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({
			name: { $nin: ['Veronica', 'Jude'] },
		});

		let results = await queryBuilder;
		expect(results.length).toEqual(1);
		expect(results[0].name).toEqual('John');
	});

	it('should be return select builder with filter string_like_percent', async () => {
		await service.create({ name: 'Only Veronica' });
		await service.create({ name: 'Veronica Silva' });
		await service.create({ name: 'Silva Veronica Silva' });
		await service.create({ name: 'John' });

		let queryBuilder = service.model.find();

		service.filters = {
			string_like_percent: {
				column: 'name',
				filter: 'string_like_percent',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			string_like_percent: 'Veronica',
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({
			name: { $regex: 'Veronica', $options: 'i' },
		});

		let results = await queryBuilder;
		expect(results.length).toEqual(3);
		expect(results[0].name).toEqual('Only Veronica');
		expect(results[1].name).toEqual('Veronica Silva');
		expect(results[2].name).toEqual('Silva Veronica Silva');
	});

	it('should be return select builder with filter string_like_percent_after', async () => {
		await service.create({ name: 'Only Veronica' });
		await service.create({ name: 'Veronica Silva' });
		await service.create({ name: 'Silva Veronica Silva' });
		await service.create({ name: 'John' });

		let queryBuilder = service.model.find();

		service.filters = {
			string_like_percent_after: {
				column: 'name',
				filter: 'string_like_percent_after',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			string_like_percent_after: 'veronica',
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({
			name: { $regex: 'veronica$', $options: 'i' },
		});

		let results = await queryBuilder;
		expect(results.length).toEqual(1);
		expect(results[0].name).toEqual('Only Veronica');
	});

	it('should be return select builder with filter string_like_percent_before', async () => {
		await service.create({ name: 'Only Veronica' });
		await service.create({ name: 'Veronica Silva' });
		await service.create({ name: 'Silva Veronica Silva' });
		await service.create({ name: 'John' });

		let queryBuilder = service.model.find();

		service.filters = {
			string_like_percent_before: {
				column: 'name',
				filter: 'string_like_percent_before',
			},
		};

		queryBuilder = service.__filters(queryBuilder, {
			string_like_percent_before: 'veronica',
		});

		// Verifica query
		let query = queryBuilder.getQuery();
		expect(queryBuilder.getQuery()).toMatchObject({
			name: { $regex: '^veronica', $options: 'i' },
		});

		let results = await queryBuilder;
		expect(results.length).toEqual(1);
		expect(results[0].name).toEqual('Veronica Silva');
	});
});
