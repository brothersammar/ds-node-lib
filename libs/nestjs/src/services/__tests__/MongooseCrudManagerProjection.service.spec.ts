import { MongooseCrudManager } from '../mongoose-crud-manager.service';
import * as mongoose from 'mongoose';
import { format } from 'date-fns';
import { IBodySearch } from '../../interfaces/crud-interfaces.interface';

export interface IUser extends mongoose.Document {
	enabled: boolean;
	name: string;
	password: string;
	age: number;
	deleted_at: string;
}

const UserSchema: mongoose.Schema = new mongoose.Schema({
	enabled: { type: Boolean },
	name: { type: String },
	password: { type: String },
	age: { type: Number },
	deleted_at: { type: String },
});

// Export the model and return your IUser interface
let Users = mongoose.model<IUser>('User', UserSchema);

let service: MongooseCrudManager<IUser>;

beforeEach(async () => {
	service = new MongooseCrudManager<IUser>(
		Users,
		// new EntityManager(null as Connection),
	);
	// service.
	await Users.deleteMany({});
});

beforeAll(async () => {
	await mongoose.connect('mongodb://admin:123456aa@localhost:9312/testing?authSource=admin', {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});
});

describe('MongooseCrudManager', () => {
	it('Projection não deve funcionar se não for enviado projection custom estiver vazio', async () => {
		await service.create({
			name: 'veronica',
			age: 1,
			password: '123456aa',
		});
		await service.create({ name: 'john', age: 2, password: '123456aa' });

		// Valores obrigatórios do projection
		service.aliases = ['name', 'age'];
		// Projections custom
		const aliases = [];

		let queryBuilder = service.model.find();
		queryBuilder = service.__projection(queryBuilder, aliases);

		// Tests
		let items = await queryBuilder;
		let item = items[0].toObject();
		// Deve conter apenas _id e name
		expect(item).toEqual({
			_id: item._id,
			name: 'veronica',
			age: 1,
			password: '123456aa',
			__v: 0,
		});
	});

	it('Projection deve retornar apenas valores enviados por parametros', async () => {
		await service.create({ name: 'A', age: 3 });
		await service.create({ name: 'B', age: 2 });
		await service.create({ name: 'C', age: 1 });
		await service.create({ name: 'D', age: 0 });

		const aliases = ['id', 'name'];

		//
		let queryBuilder = service.model.find();

		queryBuilder = service.__projection(queryBuilder, aliases);

		// Tests
		let items = await queryBuilder;
		let item = items[0].toObject();
		// Deve conter apenas _id e name
		expect(item).toEqual({
			_id: item._id,
			name: 'A',
		});
	}, 15000);

	it('Projection deve retornar paramatros custom + obrigatórios', async () => {
		await service.create({ enabled: 1, name: 'A', age: 3 });

		service.aliases = ['name', 'age'];
		const aliases = ['enabled'];

		//
		let queryBuilder = service.model.find();
		queryBuilder = service.__projection(queryBuilder, aliases);

		/**
		 * Retonar apenas os valores de
		 * age, blood_type
		 * + name
		 * */
		// Tests
		let items = await queryBuilder;
		let item = items[0].toObject();
		// Deve conter apenas _id e name
		expect(item).toEqual({
			_id: item._id,
			name: 'A',
			age: 3,
			enabled: true,
		});
	}, 15000);

	it('Projection deve ignorar valores inválidos', async () => {
		await service.create({ enabled: 1, name: 'A', age: 3 });

		service.aliases = ['name', 'id'];
		service.aliasesAllowed = ['name'];
		const aliases: any = [1000, 'school', 'name'];

		/**
		 *  Executa o teste
		 * */
		let queryBuilder = service.model.find();
		queryBuilder = service.__projection(queryBuilder, aliases);

		/**
		 * Retonar apenas os valores de
		 * + name, id
		 * */
		// Tests
		let items = await queryBuilder;
		let item = items[0].toObject();
		// Deve conter apenas _id e name
		expect(item).toEqual({
			_id: item._id,
			name: 'A',
		});
	});
});
