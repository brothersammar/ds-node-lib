import { MongooseCrudManager } from '../mongoose-crud-manager.service';
import * as mongoose from 'mongoose';
import { format } from 'date-fns';
import { IBodySearch } from '../../interfaces/crud-interfaces.interface';

export interface IUser extends mongoose.Document {
	enabled: boolean;
	name: string;
	password: string;
	age: number;
	deleted_at: string;
}

const UserSchema: mongoose.Schema = new mongoose.Schema({
	enabled: { type: Boolean },
	name: { type: String },
	password: { type: String },
	age: { type: Number },
	deleted_at: { type: String },
});

// Export the model and return your IUser interface
let Users = mongoose.model<IUser>('User', UserSchema);

let service: MongooseCrudManager<IUser>;

beforeEach(async () => {
	service = new MongooseCrudManager<IUser>(
		Users,
		// new EntityManager(null as Connection),
	);
	// service.
	await Users.deleteMany({});
});

beforeAll(async () => {
	await mongoose.connect('mongodb://admin:123456aa@localhost:9312/testing?authSource=admin', {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});
});

describe('MongooseCrudManager', () => {
	test('testar sort funcionando com 1 elemento e outro falhando', async () => {
		/**
		 * Testar sort do id em DESC
		 */
		service.sort = ['id'];

		let queryBuilder = service.model.find();

		//
		let query = service.__sorts(queryBuilder, ['-id', 'base']);
		expect(query.getOptions()).toEqual({ sort: { id: -1 } });
		//
		query = service.__sorts(queryBuilder, ['id', 'base']);
		expect(query.getOptions()).toEqual({ sort: { id: 1 } });
	});

	// Teste de objecto
	test('testar sort com alias', async () => {
		service.sort = [
			{
				name: 'favorite',
				alias: 'favorite.user_2',
			},
		];

		let queryBuilder = service.model.find();

		// Desc
		let query = service.__sorts(queryBuilder, ['-favorite', 'base']);
		expect(query.getOptions()).toEqual({ sort: { 'favorite.user_2': -1 } });
		// ASC
		query = service.__sorts(queryBuilder, ['favorite', 'base']);
		expect(query.getOptions()).toEqual({ sort: { 'favorite.user_2': 1 } });
	});

	test('testar sort com valor default', async () => {
		service.sort_default = '-id';

		let queryBuilder = service.model.find();

		//
		let query = service.__sorts(queryBuilder, ['-id', 'base']);
		expect(query.getOptions()).toEqual({ sort: { id: -1 } });
		//
		service.sort_default = 'id';
		query = service.__sorts(queryBuilder, ['id', 'base']);
		expect(query.getOptions()).toEqual({ sort: { id: 1 } });
	});
});
