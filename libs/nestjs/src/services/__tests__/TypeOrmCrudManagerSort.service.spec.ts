import {
    BaseEntity,
    Column,
    Connection,
    getConnection,
    createConnection,
    Entity,
    EntityManager,
    PrimaryGeneratedColumn,
    SelectQueryBuilder,
} from 'typeorm';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Mocks } from '../../../__tests__/__mocks__/mocks';
import { UsersStub } from './__mocks__/users-stub.entity';
import { UsersService } from './__mocks__/users-typeorm-stub.service';

describe('TypeOrmCrudManagerService', () => {
    let service: UsersService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [...Mocks.databaseTypeOrm([UsersStub]), TypeOrmModule.forFeature([UsersStub])],
            providers: [UsersService],
        }).compile();

        await UsersStub.clear();

        service = module.get<UsersService>(UsersService);
    });
    test('testar sort funcionando com 1 elemento e outro falhando', async () => {
        /**
         * Testar sort do id em DESC
         */
        service.sort = ['id'];

        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        let query = service.__sorts(queryBuilder, ['-id', 'base']);
        expect(/(.*) ORDER BY id DESC/.test(query.getQuery())).toBe(true);

        query = service.__sorts(queryBuilder, ['id', 'base']);
        expect(/(.*) ORDER BY id ASC/.test(query.getQuery())).toBe(true);
    });

    // Teste de objecto
    test('testar sort com alias', async () => {
        service.sort = [
            {
                name: 'favorite',
                alias: 'favorite.user_2',
            },
        ];

        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        // Desc
        let query = service.__sorts(queryBuilder, ['-favorite']);
        expect(/(.*) ORDER BY favorite.user_2 DESC/.test(query.getQuery())).toBe(true);

        // ASC
        query = service.__sorts(queryBuilder, ['favorite']);
        expect(/(.*) ORDER BY favorite.user_2 ASC/.test(query.getQuery())).toBe(true);
    });

    test('testar sort com valor default', async () => {
        service.sortDefault = '-id';

        const queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        // Desc
        let query = service.__sorts(queryBuilder, []);
        expect(/(.*) ORDER BY id DESC/.test(query.getQuery())).toBe(true);

        // ASC
        service.sortDefault = 'id';
        query = service.__sorts(queryBuilder, []);
        expect(/(.*) ORDER BY id ASC/.test(query.getQuery())).toBe(true);
    });
});
