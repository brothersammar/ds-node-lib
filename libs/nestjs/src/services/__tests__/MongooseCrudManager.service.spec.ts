import { MongooseCrudManager } from '../mongoose-crud-manager.service';
import * as mongoose from 'mongoose';
import { format } from 'date-fns';
import { IBodySearch } from '../../interfaces/crud-interfaces.interface';

export interface IUser extends mongoose.Document {
	enabled: boolean;
	name: string;
	password: string;
	age: number;
	deleted_at: string;
}

const UserSchema: mongoose.Schema = new mongoose.Schema({
	enabled: { type: Boolean, default: true },
	name: { type: String },
	password: { type: String },
	age: { type: Number },
	deleted_at: { type: String },
});

// Export the model and return your IUser interface
let Users = mongoose.model<IUser>('User', UserSchema);

let service: MongooseCrudManager<IUser>;

beforeEach(async () => {
	service = new MongooseCrudManager<IUser>(
		Users,
		// new EntityManager(null as Connection),
	);
	// service.
	await Users.deleteMany({});
});

beforeAll(async () => {
	await mongoose.connect('mongodb://admin:123456aa@localhost:9312/testing?authSource=admin', {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});
});

describe('MongooseCrudManager', () => {
	it('should be create model', async () => {
		let result = await service.create({
			name: 'John',
			password: '123456',
		});

		expect('John').toEqual(result.name);
		expect('123456').toEqual(result.password);
	}, 8000);

	it('should be get model', async () => {
		let model = await service.create({
			name: 'John',
			password: '123456',
		});

		let result = await service.get(model._id);

		expect(model._id.toString()).toBe(result._id.toString());
		expect('John').toEqual(result.name);
		expect('123456').toEqual(result.password);
	});

	it('should be update model', async () => {
		let model = await service.create({
			name: 'John',
			password: '123456',
		});

		let result = await service.update(model._id, {
			name: 'John',
			password: '123456',
		});

		expect(true).toBe(result);
	});

	it('should be delete model', async () => {
		let model = await service.create({
			name: 'John',
			password: '123456',
		});

		let result = await service.delete(model._id);

		expect(true).toBe(result);

		let b = await Users.findOne({
			_id: model._id,
			enabled: true,
		});

		expect(b).toBeNull();
	});

	it('should be search model', async () => {
		let model = await service.create({
			name: 'John',
			password: '123456',
		});

		let result = await service.search<any, any>(<IBodySearch<any>>{});

		expect(model._id.toString()).toBe(result.results[0]._id.toString());
		expect('John').toEqual(result.results[0].name);
		expect('123456').toEqual(result.results[0].password);
	});

	it('should be call function verifyAuthorization from create', async () => {
		service.verifyAuthorization = jest.fn().mockResolvedValue(true);

		await service.create({
			name: 'John',
			password: '123456',
		});

		expect(service.verifyAuthorization).toBeCalled();
		expect(service.verifyAuthorization).toBeCalledWith('create', {
			data: {
				name: 'John',
				password: '123456',
			},
		});
	});

	it('should be call function verifyAuthorization from update', async () => {
		service.verifyAuthorization = jest.fn().mockResolvedValue(true);

		let model = await service.create({
			name: 'John',
			password: '123456',
		});

		await service.update(model._id, {
			name: 'John',
			password: '123456',
		});

		let modelGet = await service.__get(model._id);
		expect(service.verifyAuthorization).toBeCalled();
		expect(service.verifyAuthorization).toBeCalledWith('update', {
			model: modelGet,
			data: model._id,
		});
	});

	it('should be call function verifyAuthorization from get', async () => {
		service.verifyAuthorization = jest.fn().mockResolvedValue(true);

		let model = await service.create({
			name: 'John',
			password: '123456',
		});

		await service.get(model._id);

		//
		let modelGet = await service.__get(model._id);
		expect(service.verifyAuthorization).toBeCalled();
		expect(service.verifyAuthorization).toBeCalledWith('get', {
			model: modelGet,
			data: model._id,
		});
	});

	it('should be call function verifyAuthorization from delete', async () => {
		service.verifyAuthorization = jest.fn().mockResolvedValue(true);

		let model = await service.create({
			name: 'John',
			password: '123456',
		});

		let modelGet = await service.__get(model._id);
		await service.delete(model._id);

		//
		expect(service.verifyAuthorization).toBeCalled();
		expect(service.verifyAuthorization).toBeCalledWith('delete', {
			model: modelGet,
			data: model._id,
		});
	});

	it('should be call function verifyAuthorization from search', async () => {
		service.verifyAuthorization = jest.fn().mockResolvedValue(true);

		await service.create({
			name: 'John',
			password: '123456',
		});

		await service.search({});

		expect(service.verifyAuthorization).toBeCalled();
		expect(service.verifyAuthorization).toBeCalledWith('search', {});
	});

	it('should be call function verifyAuthorization from search', async () => {
		service.verifyAuthorization = jest.fn().mockResolvedValue(true);

		await service.create({
			name: 'John',
			password: '123456',
		});

		await service.search({});

		expect(service.verifyAuthorization).toBeCalled();
		expect(service.verifyAuthorization).toBeCalledWith('search', {});
	});

	it('should be call function beforeDataHandling from create', async () => {
		service.beforeDataHandling = jest.fn().mockReturnValue({
			name: 'John2',
			password: '1234566',
		});

		let modelGet = await service.create({
			name: 'John',
			password: '123456',
		});

		let model = await service.get(modelGet._id);

		expect(service.beforeDataHandling).toBeCalled();
		expect(service.beforeDataHandling).toBeCalledWith(
			'create',
			{
				name: 'John',
				password: '123456',
			},
			{},
		);

		// Se realmente mudou o valor
		expect(model.name).toEqual('John2');
		expect(model.password).toEqual('1234566');
	});

	it('should be call function beforeDataHandling from update', async () => {
		let modelGet = await service.create({
			name: 'John',
			password: '123456',
		});

		service.beforeDataHandling = jest.fn().mockReturnValue({
			name: 'John3',
			password: 'changeNewPassword',
		});

		let result = await service.update(modelGet._id, {
			name: 'John2',
			password: 'newPassword',
		});

		let model = await service.get(modelGet._id);

		expect(service.beforeDataHandling).toBeCalled();
		expect(service.beforeDataHandling).toBeCalledWith(
			'update',
			{
				name: 'John2',
				password: 'newPassword',
			},
			{},
		);

		// Se realmente mudou o valor
		expect(model.name).toEqual('John3');
		expect(model.password).toEqual('changeNewPassword');
	});

	it('should be call function beforeDataHandling from search', async () => {
		let modelGet = await service.create({
			name: 'John',
			password: '123456',
		});

		service.beforeDataHandling = jest.fn().mockReturnValue({
			name: 'John3',
			password: 'changeNewPassword',
		});

		let result = await service.search<any, any>(<IBodySearch<any>>{});

		let model = await service.get(modelGet._id);

		expect(service.beforeDataHandling).toBeCalled();
		expect(service.beforeDataHandling).toBeCalledWith('search', {}, {});
	});

	it('should be call function beforeDataHandling from delete', async () => {
		let modelGet = await service.create({
			name: 'John',
			password: '123456',
			enabled: 1,
		});

		service.beforeDataHandling = jest.fn().mockResolvedValue({
			enabled: 0,
			deleted_at: format(new Date(), 'yyyy-MM-dd hh:mm:ss'),
		});

		await service.delete(modelGet._id);

		expect(service.beforeDataHandling).toBeCalled();
		expect(service.beforeDataHandling).toBeCalledWith(
			'search',
			{
				enabled: 0,
				deleted_at: expect.anything(),
			},
			{
				data: {
					enabled: 0,
					deleted_at: expect.anything(),
				},
				model: expect.any(Users),
			},
		);
	});

	it('should be call function beforeDataHandling from delete to change data', async () => {
		let modelGet = await service.create({
			name: 'John',
			password: '123456',
			enabled: 1,
		});

		service.beforeDataHandling = async (method, data: any) => {
			data.enabled = true;
			return data;
		};

		await service.delete(modelGet._id);

		let model = await Users.findOne({
			_id: modelGet._id,
		});

		// Se realmente mudou o valor

		expect(model.enabled).toEqual(true);
	});

	it('should be call function beforeModelHandlingSave from create', async () => {
		service.beforeModelHandlingAction = async (method, model: IUser) => {
			model.name = 'John2';
			model.password = '20';
		};

		let modelGet = await service.create({
			name: 'John',
			password: '123456',
			enabled: 1,
		});

		let model = await service.get(modelGet._id);

		// Se realmente mudou o valor
		expect(model.name).toEqual('John2');
		expect(model.password).toEqual('20');
	});

	// it('should be call function beforeModelHandlingSave from search', async () => {
	//     await service.create({
	//         name: 'John',
	//         password: '123456',
	//         enabled: 1,
	//     });

	//     service.searchHandlingQueryBuilder = async (
	//         query: SelectQueryBuilder<any>,
	//         data,
	//     ) => {
	//         query.where('id = 2');
	//     };

	//     let result = await service.search({});

	//     // Se realmente mudou o valor
	//     expect(result.results).toEqual([]);
	//     expect(result.count).toEqual(0);
	// });
});
