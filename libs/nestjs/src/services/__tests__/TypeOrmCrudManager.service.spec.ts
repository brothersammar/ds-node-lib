import {
    BaseEntity,
    Column,
    Connection,
    getConnection,
    createConnection,
    Entity,
    EntityManager,
    PrimaryGeneratedColumn,
    SelectQueryBuilder,
} from 'typeorm';
import { Test, TestingModule } from '@nestjs/testing';
import { format } from 'date-fns';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmCrudManagerService } from '../typeorm-crud-manager.service';
import { IBodySearch } from '../../interfaces/crud-interfaces.interface';
import { Mocks } from '../../../__tests__/__mocks__/mocks';
import { UsersStub } from './__mocks__/users-stub.entity';
import { UsersService } from './__mocks__/users-typeorm-stub.service';

describe('TypeOrmCrudManagerService', () => {
    let service: UsersService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [...Mocks.databaseTypeOrm([UsersStub]), TypeOrmModule.forFeature([UsersStub])],
            providers: [UsersService],
        }).compile();

        await UsersStub.clear();

        service = module.get<UsersService>(UsersService);
    });

    it('should be create model', async () => {
        const result = await service.create({
            name: 'John',
            password: '123456',
        });

        expect(1).toBe(Number(result.id));
        expect('John').toEqual(result.name);
        expect('123456').toEqual(result.password);
    });

    it('should be get model', async () => {
        await service.create({
            name: 'John',
            password: '123456',
        });

        const result = await service.get(1);

        expect(1).toBe(Number(result.id));
        expect('John').toEqual(result.name);
        expect('123456').toEqual(result.password);
    });

    it('should be update model', async () => {
        await service.create({
            name: 'John',
            password: '123456',
        });

        const result = await service.update(1, {
            name: 'John',
            password: '123456',
        });

        expect(true).toBe(result);
    });

    it('should be delete model', async () => {
        await service.create({
            name: 'John',
            password: '123456',
        });

        const result = await service.delete(1);

        expect(true).toBe(result);

        // Enable = false
        const user = await UsersStub.findOne(1);
        expect(0).toBe(user.enabled);
    });

    it('should be search model', async () => {
        await service.create({
            name: 'John',
            password: '123456',
        });

        const result = await service.search<any, any>({} as IBodySearch<any>);

        expect(1).toBe(Number(result.results[0].id));
        expect('John').toEqual(result.results[0].name);
        expect('123456').toEqual(result.results[0].password);
    });

    it('should be call function verifyAuthorization from create', async () => {
        service.verifyAuthorization = jest.fn().mockResolvedValue(true);

        await service.create({
            name: 'John',
            password: '123456',
        });

        expect(service.verifyAuthorization).toHaveBeenCalled();
        expect(service.verifyAuthorization).toHaveBeenCalledWith('create', {
            data: {
                name: 'John',
                password: '123456',
            },
        });
    });

    it('should be call function verifyAuthorization from update', async () => {
        service.verifyAuthorization = jest.fn().mockResolvedValue(true);

        await service.create({
            name: 'John',
            password: '123456',
        });

        await service.update(1, {
            name: 'John',
            password: '123456',
        });

        expect(service.verifyAuthorization).toHaveBeenCalled();
        expect(service.verifyAuthorization).toHaveBeenCalledWith('update', {
            model: {
                deletedAt: null,
                enabled: 1,
                id: '1',
                name: 'John',
                password: '123456',
                age: 1,
                bloodType: 10,
            },
            data: 1,
        });
    });

    it('should be call function verifyAuthorization from get', async () => {
        service.verifyAuthorization = jest.fn().mockResolvedValue(true);

        await service.create({
            name: 'John',
            password: '123456',
        });

        await service.get(1);

        expect(service.verifyAuthorization).toHaveBeenCalled();
        expect(service.verifyAuthorization).toHaveBeenCalledWith('get', {
            model: {
                deletedAt: null,
                enabled: 1,
                id: '1',
                name: 'John',
                password: '123456',
                age: 1,
                bloodType: 10,
            },
            data: 1,
        });
    });

    it('should be call function verifyAuthorization from delete', async () => {
        service.verifyAuthorization = jest.fn().mockResolvedValue(true);

        await service.create({
            name: 'John',
            password: '123456',
        });

        await service.delete(1);

        expect(service.verifyAuthorization).toHaveBeenCalled();
        expect(service.verifyAuthorization).toHaveBeenCalledWith('delete', {
            model: {
                deletedAt: null,
                enabled: 1,
                id: '1',
                name: 'John',
                password: '123456',
                age: 1,
                bloodType: 10,
            },
            data: 1,
        });
    });

    it('should be call function verifyAuthorization from search', async () => {
        service.verifyAuthorization = jest.fn().mockResolvedValue(true);

        await service.create({
            name: 'John',
            password: '123456',
        });

        await service.search({});

        expect(service.verifyAuthorization).toHaveBeenCalled();
        expect(service.verifyAuthorization).toHaveBeenCalledWith('search', {});
    });

    it('should be call function verifyAuthorization from search', async () => {
        service.verifyAuthorization = jest.fn().mockResolvedValue(true);

        await service.create({
            name: 'John',
            password: '123456',
        });

        await service.search({});

        expect(service.verifyAuthorization).toHaveBeenCalled();
        expect(service.verifyAuthorization).toHaveBeenCalledWith('search', {});
    });

    it('should be call function beforeDataHandling from create', async () => {
        service.beforeDataHandling = jest.fn().mockReturnValue({
            name: 'John2',
            password: '1234566',
        });

        await service.create({
            name: 'John',
            password: '123456',
        });

        const model = await service.get(1);

        expect(service.beforeDataHandling).toHaveBeenCalled();
        expect(service.beforeDataHandling).toHaveBeenCalledWith(
            'create',
            {
                name: 'John',
                password: '123456',
            },
            {},
        );

        // Se realmente mudou o valor
        expect(model.name).toEqual('John2');
        expect(model.password).toEqual('1234566');
    });

    it('should be call function beforeDataHandling from update', async () => {
        await service.create({
            name: 'John',
            password: '123456',
        });

        service.beforeDataHandling = jest.fn().mockReturnValue({
            name: 'John3',
            password: 'changeNewPassword',
        });

        const result = await service.update(1, {
            name: 'John2',
            password: 'newPassword',
        });

        const model = await service.get(1);

        expect(service.beforeDataHandling).toHaveBeenCalled();
        expect(service.beforeDataHandling).toHaveBeenCalledWith(
            'update',
            {
                name: 'John2',
                password: 'newPassword',
            },
            {
                data: {
                    name: 'John2',
                    password: 'newPassword',
                },
                model: expect.any(UsersStub),
            },
        );

        // Se realmente mudou o valor
        expect(model.name).toEqual('John3');
        expect(model.password).toEqual('changeNewPassword');
    });

    it('should be call function beforeDataHandling from search', async () => {
        await service.create({
            name: 'John',
            password: '123456',
        });

        service.beforeDataHandling = jest.fn().mockReturnValue({
            name: 'John3',
            password: 'changeNewPassword',
        });

        const result = await service.search<any, any>({} as IBodySearch<any>);

        const model = await service.get(1);

        expect(service.beforeDataHandling).toHaveBeenCalled();
        expect(service.beforeDataHandling).toHaveBeenCalledWith('search', {}, {});
    });

    it('should be call function beforeDataHandling from delete', async () => {
        await service.create({
            name: 'John',
            password: '123456',
            enabled: 1,
        });

        service.beforeDataHandling = jest.fn().mockResolvedValue({
            enabled: 0,
            deletedAt: format(new Date(), 'yyyy-MM-dd hh:mm:ss'),
        });

        await service.delete(1);

        expect(service.beforeDataHandling).toHaveBeenCalled();
        expect(service.beforeDataHandling).toHaveBeenCalledWith(
            'search',
            {
                enabled: 0,
                deletedAt: expect.anything(),
            },
            {
                data: {
                    enabled: 0,
                    deletedAt: expect.anything(),
                },
                model: expect.any(UsersStub),
            },
        );
    });

    it('should be call function beforeDataHandling from delete to change data', async () => {
        await service.create({
            name: 'John',
            password: '123456',
            enabled: 1,
        });

        service.beforeDataHandling = async (method, data: any) => {
            data.enabled = 2;
            return data;
        };

        await service.delete(1);

        const model = await UsersStub.findOne(1);

        // Se realmente mudou o valor

        expect(model.enabled).toEqual(2);
    });

    it('should be call function beforeModelHandlingSave from create', async () => {
        service.beforeModelHandlingAction = async (method, model: UsersStub) => {
            model.name = 'John2';
            model.password = '20';
        };

        await service.create({
            name: 'John',
            password: '123456',
            enabled: 1,
        });

        const model = await service.get(1);

        // Se realmente mudou o valor
        expect(model.name).toEqual('John2');
        expect(model.password).toEqual('20');
    });

    it('should be call function beforeModelHandlingSave from search', async () => {
        await service.create({
            name: 'John',
            password: '123456',
            enabled: 1,
        });

        service.searchHandlingQueryBuilder = async (query: SelectQueryBuilder<any>, data) => {
            query.where('id = 2');
        };

        const result = await service.search({});

        // Se realmente mudou o valor
        expect(result.results).toEqual([]);
        expect(result.count).toEqual(0);
    });
});

describe('TypeOrmCrudManagerService - Filters', () => {
    let service: UsersService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [...Mocks.databaseTypeOrm([UsersStub]), TypeOrmModule.forFeature([UsersStub])],
            providers: [UsersService],
        }).compile();

        await UsersStub.clear();

        service = module.get<UsersService>(UsersService);
    });

    async function createUsers(): Promise<any> {
        await service.create({
            name: 'John',
            password: 'password',
        });
        await service.create({
            name: 'John1',
            password: 'password1',
        });
        await service.create({
            name: 'John2',
            password: 'password2',
        });
        await service.create({
            name: 'John3',
            password: 'password3',
        });
    }

    it('should be return select builder with filters', async () => {
        let queryBuilder = getConnection()
            .getRepository(UsersStub)
            .createQueryBuilder();

        service.filters = {
            int: {
                column: 'int',
                filter: 'int',
            },
            intGt: {
                column: 'intGt',
                filter: 'intGt',
            },
            intLt: {
                column: 'intLt',
                filter: 'intLt',
            },
            intGte: {
                column: 'intGte',
                filter: 'intGte',
            },
            intLte: {
                column: 'intLte',
                filter: 'intLte',
            },
            string: {
                column: 'string',
                filter: 'string',
            },
            stringLike: {
                column: 'stringLike',
                filter: 'stringLike',
            },
            arrayInt: {
                column: 'arrayInt',
                filter: 'arrayInt',
            },
            arrayIntNot: {
                column: 'arrayIntNot',
                filter: 'arrayIntNot',
            },
            arrayString: {
                column: 'arrayString',
                filter: 'arrayString',
            },
            arrayStringNot: {
                column: 'arrayStringNot',
                filter: 'arrayStringNot',
            },
            stringLikeNot: {
                column: 'stringLikeNot',
                filter: 'stringLikeNot',
            },
            stringLikePercent: {
                column: 'stringLikePercent',
                filter: 'stringLikePercent',
            },
            stringLikePercentAfter: {
                column: 'stringLikePercentAfter',
                filter: 'stringLikePercentAfter',
            },
            stringLikePercentBefore: {
                column: 'stringLikePercentBefore',
                filter: 'stringLikePercentBefore',
            },
            intInColumn: {
                column: 'intInColumn',
                filter: 'intInColumn',
            },
        };

        queryBuilder = service.__filters(queryBuilder, {
            int: 1,
            intGt: 1,
            intLt: 1,
            intGte: 1,
            intLte: 1,
            string: 'string',
            stringLike: 'string',
            arrayInt: [0, 1],
            arrayIntNot: [0, 1],
            arrayString: ['word'],
            arrayStringNot: ['word'],
            stringLikeNot: 'string',
            stringLikePercent: 'string',
            stringLikePercentAfter: 'string',
            stringLikePercentBefore: 'string',
            intInColumn: [1],
        });

        expect(queryBuilder.getQuery()).toContain('int = :int');
        expect(queryBuilder.getQuery()).toContain('intGt > :intGt');
        expect(queryBuilder.getQuery()).toContain('intLt < :intLt');
        expect(queryBuilder.getQuery()).toContain('intGte >= :intGte');
        expect(queryBuilder.getQuery()).toContain('intLte <= :intLte');
        expect(queryBuilder.getQuery()).toContain('string = :string');
        expect(queryBuilder.getQuery()).toContain('stringLike LIKE :stringLike');
        expect(queryBuilder.getQuery()).toContain('arrayInt IN (:arrayInt)');
        expect(queryBuilder.getQuery()).toContain('arrayIntNot NOT IN (:arrayIntNot)');
        expect(queryBuilder.getQuery()).toContain('arrayString IN (:arrayString)');
        expect(queryBuilder.getQuery()).toContain('arrayStringNot NOT IN (:arrayStringNot)');
        expect(queryBuilder.getQuery()).toContain('stringLikeNot NOT LIKE :stringLikeNot');
        expect(queryBuilder.getQuery()).toContain('stringLikePercent LIKE :stringLikePercent');
        expect(queryBuilder.getQuery()).toContain('stringLikePercentAfter LIKE :stringLikePercentAfter');
        expect(queryBuilder.getQuery()).toContain('stringLikePercentBefore LIKE :stringLikePercentBefore');
        expect(queryBuilder.getQuery()).toContain('FIND_IN_SET(:intInColumn, intInColumn) = 0');

        expect(queryBuilder.getParameters().int).toEqual(1);
        expect(queryBuilder.getParameters().intGt).toEqual(1);
        expect(queryBuilder.getParameters().intLt).toEqual(1);
        expect(queryBuilder.getParameters().intGte).toEqual(1);
        expect(queryBuilder.getParameters().intLte).toEqual(1);
        expect(queryBuilder.getParameters().string).toEqual('string');
        expect(queryBuilder.getParameters().stringLike).toEqual('string');
        expect(queryBuilder.getParameters().arrayInt).toEqual([0, 1]);
        expect(queryBuilder.getParameters().arrayIntNot).toEqual([0, 1]);
        expect(queryBuilder.getParameters().arrayString).toEqual(['word']);
        expect(queryBuilder.getParameters().arrayStringNot).toEqual(['word']);
        expect(queryBuilder.getParameters().stringLikeNot).toEqual('string');
        expect(queryBuilder.getParameters().stringLikePercent).toEqual('%string%');
        expect(queryBuilder.getParameters().stringLikePercentAfter).toEqual('string%');
        expect(queryBuilder.getParameters().stringLikePercentBefore).toEqual('%string');
        expect(queryBuilder.getParameters().intInColumn).toEqual([1]);
    });
});
