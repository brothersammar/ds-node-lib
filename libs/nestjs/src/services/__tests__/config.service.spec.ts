import * as Joi from '@hapi/joi';
import { ConfigSetup } from '../config.service';
import ExampleConfigSetup from './__mocks__/config-import-stub';

describe('ConfigSetup', () => {
    test('deve instanciar corretamente config de exemplo', async () => {
        expect(ExampleConfigSetup.PORT).toBe(3000);
        expect(ExampleConfigSetup.NODE_ENV).toBe('development');
        expect(ExampleConfigSetup.API_AUTH_ENABLED).toBe(true);
    });
});
