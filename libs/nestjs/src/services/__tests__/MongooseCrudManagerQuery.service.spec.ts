import { MongooseCrudManager } from '../mongoose-crud-manager.service';
import * as mongoose from 'mongoose';
import { format } from 'date-fns';
import { IBodySearch } from '../../interfaces/crud-interfaces.interface';

export interface IUser extends mongoose.Document {
	enabled: boolean;
	name: string;
	password: string;
	age: number;
	deleted_at: string;
}

const UserSchema: mongoose.Schema = new mongoose.Schema({
	enabled: { type: Boolean },
	name: { type: String },
	password: { type: String },
	age: { type: Number },
	deleted_at: { type: String },
});

// Export the model and return your IUser interface
let Users = mongoose.model<IUser>('User', UserSchema);

let service: MongooseCrudManager<IUser>;

beforeEach(async () => {
	service = new MongooseCrudManager<IUser>(
		Users,
		// new EntityManager(null as Connection),
	);
	// service.
	await Users.deleteMany({});
});

beforeAll(async () => {
	await mongoose.connect('mongodb://admin:123456aa@localhost:9312/testing?authSource=admin', {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});
});

describe('MongooseCrudManager', () => {
	it('offset e filters', async () => {
		const filters = {
			limit: 3,
			offset: 4,
		};

		/**
		 * Teste da funcao
		 */
		let queryBuilder = service.model.find();

		//
		const query = service.__query(queryBuilder, filters);
		expect(query.getOptions()).toMatchObject({ limit: 3 });
		expect(query.getOptions()).toMatchObject({ skip: 4 });
	}, 15000);

	it('only offset', async () => {
		const filters = {
			offset: 1,
		};

		/**
		 * Teste da funcao
		 */
		let queryBuilder = service.model.find();

		//
		const query = service.__query(queryBuilder, filters);
		expect(query.getOptions()).toMatchObject({ skip: 1 });
	}, 15000);

	it('only limit', async () => {
		const filters = {
			limit: 4,
		};

		/**
		 * Teste da funcao
		 */
		let queryBuilder = service.model.find();

		//
		const query = service.__query(queryBuilder, filters);
		expect(query.getOptions()).toMatchObject({ limit: 4 });
	}, 15000);

	it('Limit não pode exceder limitMax', async () => {
		// Limite máximo do valor limite
		service.limitMax = 10;

		const filters = {
			limit: 15,
		};

		/**
		 * Teste da funcao
		 */
		let queryBuilder = service.model.find();

		//
		const query = service.__query(queryBuilder, filters);
		expect(query.getOptions()).toMatchObject({ limit: 10 });
	}, 15000);

	it('sem nenhum valor', async () => {
		const filters = {};

		/**
		 * Teste da funcao
		 */
		let queryBuilder = service.model.find();

		//
		const query = service.__query(queryBuilder, filters);
		expect(query.getOptions()).toMatchObject({ limit: 20 });
	}, 15000);

	it('um valor inválido', async () => {
		const filters = {
			limit: -100,
			offset: -50,
		};

		/**
		 * Teste da funcao
		 */
		let queryBuilder = service.model.find();

		//
		const query = service.__query(queryBuilder, filters);
		expect(query.getOptions()).toMatchObject({ limit: 20 });
	}, 15000);

	it('um valor invalido - não número', async () => {
		const filters = {
			limit: 'sdsd',
			offset: 'sdsd',
		};

		/**
		 * Teste da funcao
		 */
		let queryBuilder = service.model.find();

		//
		const query = service.__query(queryBuilder, filters);
		expect(query.getOptions()).toMatchObject({ limit: 20 });
	}, 15000);

	it('should be change offset with page 1', async () => {
		const filters = {
			page: 1,
		};

		/**
		 * Teste da funcao
		 */
		let queryBuilder = service.model.find();

		//
		const query = service.__query(queryBuilder, filters);
		expect(query.getOptions()).toMatchObject({ limit: 20 });
	}, 15000);

	it('should be change offset with page 1', async () => {
		const filters = {
			page: 3,
		};

		/**
		 * Teste da funcao
		 */
		let queryBuilder = service.model.find();

		//
		const query = service.__query(queryBuilder, filters);
		expect(query.getOptions()).toMatchObject({ limit: 20 });
		expect(query.getOptions()).toMatchObject({ skip: 40 });
	}, 15000);
});
