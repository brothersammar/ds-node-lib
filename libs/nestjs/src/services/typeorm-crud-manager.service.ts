import { BaseEntity, EntityManager, getConnection, SelectQueryBuilder, Repository, DeepPartial, Entity } from 'typeorm';
import { format } from 'date-fns';
import { NotFoundException, UnauthorizedException } from '@nestjs/common';
import { Helpers } from '@devesharp/common';
import {
    IBodySearch,
    IResultSearch,
    IArgsMethod,
    IFilter,
    ISortSet,
    IQuery,
} from '../interfaces/crud-interfaces.interface';

export class TypeOrmCrudManagerService<T extends BaseEntity, AuthClass> {
    /**
     * Entidade de autentificação
     */
    auth: AuthClass | boolean = false;

    /**
     * Valores que podem ser ordenados
     */
    sort: (ISortSet | string)[] = [];

    /**
     * Valor padrão de ordem
     */
    sortDefault = '';

    /**
     * Limite máximo, para não dar crash no banco de dados
     */
    limitMax = 100;

    /**
     * Limite padrão
     */
    limitDefault = 20;

    /**
     * Valores que podem ser filtrados
     */
    filters: { [key: string]: IFilter } = {};

    /**
     * Campos que devem ser retornados
     */
    fields = [];

    /**
     * Query
     */
    query = {};

    /**
     * Projections obrigátórios do banco de dados
     */
    aliases = [];

    /**
     * Projections permitidos
     */
    aliasesAllowed = [];

    transaction = false;

    transactionalEntityManager: EntityManager;

    /**
     * Model
     */
    instance: Repository<T>;

    /**
     * EntityManager
     */
    entityManager: EntityManager;

    constructor(instance: Repository<T>) {
        this.instance = instance;
        // this.entityManager = entityManager || getConnection().manager;
        this.entityManager = getConnection().manager;
    }

    /**
     * Adicionar entidade de autentificação
     * @param auth
     */
    addAuth(auth?: AuthClass | boolean): this {
        this.auth = auth || false;
        return this;
    }

    /**
     * Verifica se usuário tem autorização para utilizar a rota
     * @param method
     * @param args
     */
    verifyAuthorization(method: 'create' | 'get' | 'update' | 'search' | 'delete', args: IArgsMethod<T>) {
        return true;
    }

    /**
     * Tratamento de dados no inicio do metodo
     * @param method
     * @param data
     * @param args
     */
    beforeDataHandling(
        method: 'create' | 'get' | 'update' | 'search' | 'delete',
        data: any,
        args: IArgsMethod<T>,
    ): Promise<any> {
        return data;
    }

    /**
     * Tratamento de dados antes da ação(salvar, buscar, criar) do metodo
     * @param method
     * @param data
     * @param args
     */
    async beforeModelHandlingAction(
        method: 'create' | 'get' | 'update' | 'search' | 'delete',
        model: T,
        args: IArgsMethod<T>,
    ): Promise<any> {
        //
    }

    /**
     * Tratamento da Query
     * @param method
     * @param data
     * @param args
     */
    async searchHandlingQueryBuilder(query: SelectQueryBuilder<any>, args: IArgsMethod<T>): Promise<any> {
        //
    }

    /**
     * Criar Model
     * @param data
     */
    async create<B>(_data: B): Promise<any> {
        /**
         * Verificar autorização
         */
        if (!this.verifyAuthorization('create', { data: _data })) {
            throw new UnauthorizedException();
        }

        /**
         * Tratamento de dados
         */
        const data = await this.beforeDataHandling('create', _data, {});

        /**
         * Cria instancia
         */
        const model = this.instance.create(data as DeepPartial<T>);

        /**
         * Tratamento do modelo
         */
        await this.beforeModelHandlingAction('create', model, {
            data,
            model,
        });

        return this.entityManager.save(model);
    }

    /**
     * Resgatar Model
     * @param id
     */
    async get(id: number): Promise<any> {
        const model = await this.instance.findOne({
            where: {
                id,
                enabled: 1,
            },
        });

        /**
         * Não encontrado
         */
        if (!model) throw new NotFoundException();

        /**
         * Verificar autorização
         */
        if (!this.verifyAuthorization('get', { model: model as T, data: id })) {
            throw new UnauthorizedException();
        }

        return model;
    }

    /**
     * Atualizar model
     * @param id
     * @param data
     */
    async update<B>(id: number, _data: B): Promise<boolean> {
        const model = await this.getModel(id);

        /**
         * Verificar autorização
         */
        if (!this.verifyAuthorization('update', { model: model as T, data: id })) {
            throw new UnauthorizedException();
        }

        /**
         * Tratamento de dados
         */
        const data = await this.beforeDataHandling('update', _data, {
            data: _data,
            model,
        });

        return this.updateModel((model as any).id, data);
    }

    /**
     * Delete model
     * @param id
     */
    async delete(id: number): Promise<boolean> {
        const model = await this.getModel(id);

        /**
         * Verificar autorização
         */
        if (!this.verifyAuthorization('delete', { model: model as T, data: id })) {
            throw new UnauthorizedException();
        }

        let data = {
            enabled: 0,
            deletedAt: format(new Date(), 'yyyy-MM-dd hh:mm:ss'),
        };

        /**
         * Tratamento do modelo
         */
        data = await this.beforeDataHandling('search', data, {
            data,
            model,
        });

        return this.updateModel((model as any).id, data);
    }

    /**
     * Buscar model
     * @param data
     */
    async search<B, B2>(_data: IBodySearch<B>): Promise<IResultSearch<B2>> {
        /**
         * Verificar autorização
         */
        if (!this.verifyAuthorization('search', {})) {
            throw new UnauthorizedException();
        }

        /**
         * Tratamento de dados
         */
        const data = await this.beforeDataHandling('search', _data, {});

        /**
         * Criar search
         */
        const queryBuilder = this.__search(data.filters, data.query, data.alias);

        /**
         * Tratamento de Query Builder
         */
        await this.searchHandlingQueryBuilder(queryBuilder, {
            data,
        });

        const results: IResultSearch<B2> = {
            count: 0,
            results: [],
        };

        if (data && data.query && data.query.limit === 0) {
            results.results = [];
        } else {
            results.results = await this.__getRawMany(queryBuilder);
        }

        results.count = await queryBuilder.getCount();

        return results;
    }

    /**
     * Obter modelo
     * @param body
     */
    protected async getModel(id: number): Promise<T> {
        const model = await this.instance.findOne({
            where: {
                id,
                enabled: 1,
            },
        });
        return Promise.resolve(model as T);
    }

    /**
     * Atualizar Modelo com dados enviados
     * @param id
     * @param body
     * @private
     */
    protected async updateModel(id: number, body: any): Promise<boolean> {
        const modelUpdate = await this.entityManager.update(this.instance.metadata.name, id, body);
        return Promise.resolve(!!modelUpdate.raw.affectedRows);
    }

    /**
     * Lógica para filtrar model da DB
     * @param filters
     * @param query
     * @param alias
     */
    protected __search(_filters: any, _query: any, alias: string[]): SelectQueryBuilder<any> {
        let queryBuilder = this.instance.createQueryBuilder();

        let query = _query;
        let filters = _filters;

        if (!query) query = {};
        if (!filters) filters = {};

        queryBuilder = this.__filters(queryBuilder, filters || {});
        queryBuilder = this.__query(queryBuilder, query || {});
        queryBuilder = this.__sorts(queryBuilder, query.sorts || []);
        queryBuilder = this.__projection(queryBuilder, alias || []);

        return queryBuilder;
    }

    /**
     * Remove aliass ModelsName__ dos resultados
     * @param documentQuery
     * @private
     */
    private async __getRawMany(documentQuery: SelectQueryBuilder<any>) {
        const results = await documentQuery.getRawMany();
        const newResults = [];

        results.forEach(row => {
            const newRow = {};

            Object.entries(row).forEach(values => {
                const [key, value] = values;
                newRow[key.replace(`${this.instance.metadata.name}_`, '')] = value;
            });

            newResults.push(newRow);
        });

        return newResults;
    }

    /**
     * Define limit, offset
     * @param resource
     * @param query
     */
    public __query(resource: SelectQueryBuilder<any>, query: IQuery): SelectQueryBuilder<any> {
        let limit = this.limitDefault;
        if (query.limit && !Number.isNaN(Number(query.limit)) && query.limit > 0) {
            resource.limit(Math.min(this.limitMax, Number(query.limit)));
            limit = Math.min(this.limitMax, Number(query.limit));
        } else {
            resource.limit(this.limitDefault);
        }

        if (query.offset && !Number.isNaN(Number(query.offset)) && query.offset > 0) {
            resource.offset(Number(query.offset));
        }

        if (query.page && !Number.isNaN(Number(query.page)) && query.page > 0) {
            resource.offset((Number(query.page) - 1) * limit);
        }

        return resource;
    }

    /**
     * Define quais valores devem ser retornados na busca do banco de dados
     * @param resource
     * @param projections
     */
    public __projection(resource: SelectQueryBuilder<any>, projections: string[]): SelectQueryBuilder<any> {
        const newProj = [];

        /**
         * Adicionar projections enviados pela requisição
         * Caso não seja enviado nenhum valor de projections
         * DB deve retornar todos os valores
         */
        if (projections.length > 0) {
            projections.forEach(element => {
                /**
                 * Precisa estar entre aliasesAllowed
                 * Ou se aliasesAllowed estiver vazio, permite todos
                 */
                if (
                    typeof element === 'string' &&
                    (this.aliasesAllowed.indexOf(element) !== -1 || !this.aliasesAllowed.length)
                ) {
                    newProj.push(`\`${this.instance.metadata.name.toString()}\`.\`${element}\``);
                }
            });

            /**
             * Executar merge em cima dos projections enviados pela requisição
             */
            if (this.aliases.length > 0) {
                this.aliases.forEach(element => {
                    if (typeof element === 'string') {
                        newProj.push(`\`${this.instance.metadata.name.toString()}\`.\`${element}\``);
                    }
                });
            }

            /**
             * Executa select
             */
            resource = resource.select(newProj);
        }

        return resource;
    }

    /**
     * Define ordem dos resultados
     * Verifica se valores enviados estão disponiveis para uso
     * @param resource
     * @param sortVal
     */
    public __sorts(resource: SelectQueryBuilder<any>, sortVal: string[]): SelectQueryBuilder<any> {
        // Preparar o resource com o sort
        const data = {
            sort: {},
        };

        /**
         * Verificar se os itens enviados para o servidor estão na lista de sort
         */
        for (let i = 0; i < this.sort.length; i += 1) {
            const mainSort = this.sort[i];

            for (let iS = 0; iS < sortVal.length; iS += 1) {
                const reqSort = sortVal[iS];
                let checkSort = sortVal[iS].toString();

                /**
                 * Se valor for uma script apenas compara
                 */
                if (typeof mainSort === 'string') {
                    if (reqSort.indexOf('-') === 0) {
                        checkSort = reqSort.substring(1);
                    }

                    if (checkSort === mainSort) {
                        if (reqSort.indexOf('-') === 0) {
                            resource.addOrderBy(checkSort, 'DESC');
                        } else {
                            resource.addOrderBy(checkSort, 'ASC');
                        }
                    }

                    /**
                     * Se o elemento de sort não for uma string
                     * verificar valores que existem dentro do objeto
                     */
                } else if (typeof mainSort === 'object') {
                    if (reqSort.indexOf('-') === 0) {
                        checkSort = reqSort.substring(1);
                    }

                    if (mainSort.name === checkSort) {
                        if (reqSort.indexOf('-') === 0) {
                            resource.addOrderBy(mainSort.alias, 'DESC');
                        } else {
                            resource.addOrderBy(mainSort.alias, 'ASC');
                        }
                    }
                }
            }
        }

        // Caso nao tenha nada para ser sorteado sera enviado o sort default
        if (sortVal.length === 0 && this.sortDefault !== '') {
            if (this.sortDefault.indexOf('-') === 0) {
                resource.addOrderBy(this.sortDefault.substr(1), 'DESC');
            } else {
                resource.addOrderBy(this.sortDefault, 'ASC');
            }
        }

        return resource;
    }

    public __count(resource: SelectQueryBuilder<any>): Promise<number> {
        return resource
            .limit()
            .offset(0)
            .getCount();
    }

    /**
     * Define os filtros que serão usados na pesquisa
     * no Model Mongoose enviado
     * @param resource
     * @param filters
     */
    public __filters(resource: SelectQueryBuilder<any>, filters: { [key: string]: any }): SelectQueryBuilder<any> {
        /**
         * Filters Allowed
         */
        Object.entries(this.filters).forEach(valueThisFilters => {
            const [key, filterConfig] = valueThisFilters;

            Object.entries(filters).forEach(valueFilters => {
                const [key2, filterItem] = valueFilters;

                /**
                 * Compara filters com filtersallowed
                 */
                if (key === key2) {
                    switch (filterConfig.filter) {
                        case 'int':
                            resource.andWhere(`${filterConfig.column} = :${key}`, {
                                [key]: Number(filterItem),
                            });
                            break;

                        case 'intGt':
                            resource.andWhere(`${filterConfig.column} > :${key}`, {
                                [key]: Number(filterItem),
                            });
                            break;

                        case 'intLt':
                            resource.andWhere(`${filterConfig.column} < :${key}`, {
                                [key]: Number(filterItem),
                            });
                            break;

                        case 'intGte':
                            resource.andWhere(`${filterConfig.column} >= :${key}`, {
                                [key]: Number(filterItem),
                            });

                            break;

                        case 'intLte':
                            resource.andWhere(`${filterConfig.column} <= :${key}`, {
                                [key]: Number(filterItem),
                            });
                            break;

                        case 'string':
                            resource.andWhere(`${filterConfig.column} = :${key}`, {
                                [key]: filterItem,
                            });
                            break;

                        case 'stringLike':
                            resource.andWhere(`${filterConfig.column} LIKE :${key}`, {
                                [key]: filterItem,
                            });
                            break;

                        case 'arrayInt':
                            if (Helpers.typeOf(filterItem) === 'array') {
                                resource.andWhere(`${filterConfig.column} IN (:${key})`, {
                                    [key]: filterItem.map(Number),
                                });
                            }
                            break;

                        case 'arrayIntNot':
                            if (Helpers.typeOf(filterItem) === 'array') {
                                resource.andWhere(`${filterConfig.column} NOT IN (:${key})`, {
                                    [key]: filterItem.map(Number),
                                });
                            }
                            break;

                        case 'arrayString':
                            if (Helpers.typeOf(filterItem) === 'array') {
                                resource.andWhere(`${filterConfig.column} IN (:${key})`, {
                                    [key]: filterItem,
                                });
                            }
                            break;

                        case 'arrayStringNot':
                            if (Helpers.typeOf(filterItem) === 'array') {
                                resource.andWhere(`${filterConfig.column} NOT IN (:${key})`, {
                                    [key]: filterItem,
                                });
                            }
                            break;

                        case 'stringLikeNot':
                            resource.andWhere(`${filterConfig.column} NOT LIKE :${key}`, {
                                [key]: filterItem,
                            });
                            break;

                        case 'stringLikePercent':
                            resource.andWhere(`${filterConfig.column} LIKE :${key}`, {
                                [key]: `%${filterItem}%`,
                            });
                            break;

                        case 'stringLikePercentAfter':
                            resource.andWhere(`${filterConfig.column} LIKE :${key}`, {
                                [key]: `${filterItem}%`,
                            });
                            break;

                        case 'stringLikePercentBefore':
                            resource.andWhere(`${filterConfig.column} LIKE :${key}`, {
                                [key]: `%${filterItem}`,
                            });
                            break;

                        case 'intInColumn':
                            if (Helpers.typeOf(filterItem) === 'array') {
                                resource.andWhere(`FIND_IN_SET(:${key}, ${filterConfig.column}) = 0`, {
                                    [key]: filterItem.map(Number),
                                });
                            }
                            break;

                        default:
                            throw Error(`${filterConfig.filter} filter not defined`);
                    }
                }
            });
        });

        return resource;
    }
}
