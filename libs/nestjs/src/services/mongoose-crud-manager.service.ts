import { Model, Document, DocumentQuery } from 'mongoose';
import { format } from 'date-fns';
import { NotFoundException, UnauthorizedException } from '@nestjs/common';
import { Helpers } from '@devesharp/common';
import {
    IBodySearch,
    IResultSearch,
    IArgsMethod,
    IFilter,
    ISortSet,
    IQuery,
} from '../interfaces/crud-interfaces.interface';

export class MongooseCrudManager<T extends Document> {
    /**
     * Valores que podem ser ordenados
     */
    sort: (ISortSet | string)[] = [];

    /**
     * Valor padrão de ordem
     */
    sortDefault = '';

    /**
     * Limite máximo, para não dar crash no banco de dados
     */
    limitMax = 100;

    /**
     * Limite padrão
     */
    limitDefault = 20;

    /**
     * Valores que podem ser filtrados
     */
    filters: { [key: string]: IFilter } = {};

    /**
     * Campos que devem ser retornados
     */
    fields = [];

    /**
     * Query
     */
    query = {};

    /**
     * Projections obrigátórios do banco de dados
     */
    aliases = [];

    /**
     * Projections permitidos
     */
    aliasesAllowed = [];

    /**
     * Model
     */
    model: typeof Model;

    constructor(model: typeof Model) {
        this.model = model;
    }

    /**
     * Verifica se usuário tem autorização para utilizar a rota
     * @param method
     * @param args
     */
    verifyAuthorization(method: 'create' | 'get' | 'update' | 'search' | 'delete', args: IArgsMethod<T>): boolean {
        return true;
    }

    /**
     * Tratamento de dados no inicio do metodo
     * @param method
     * @param data
     * @param args
     */
    beforeDataHandling(
        method: 'create' | 'get' | 'update' | 'search' | 'delete',
        data: any,
        args: IArgsMethod<T>,
    ): Promise<any> {
        return data;
    }

    /**
     * Tratamento de dados antes da ação(salvar, buscar, criar) do metodo
     * @param method
     * @param data
     * @param args
     */
    async beforeModelHandlingAction(
        method: 'create' | 'get' | 'update' | 'search' | 'delete',
        model: T,
        args: IArgsMethod<T>,
    ): Promise<any> {
        //
    }

    /**
     * Tratamento da Query
     * @param method
     * @param data
     * @param args
     */
    async searchHandlingQueryBuilder(query: DocumentQuery<any, any>, args: IArgsMethod<T>): Promise<any> {
        //
    }

    /**
     * Criar Model
     * @param data
     */
    async create<B>(_data: B): Promise<any> {
        /**
         * Verificar autorização
         */
        if (!this.verifyAuthorization('create', { data: _data })) {
            throw new UnauthorizedException();
        }

        /**
         * Tratamento de dados
         */
        const data = await this.beforeDataHandling('create', _data, {});

        /**
         * Cria instancia
         */
        let model = new this.model(data);

        /**
         * Tratamento do modelo
         */
        await this.beforeModelHandlingAction('create', model, {
            data,
            model,
        });

        model = await model.save();

        return model;
    }

    /**
     * Resgatar Model
     * @param id
     */
    async get(_id: string): Promise<any> {
        const model = await this.__get(_id);

        /**
         * Não encontrado
         */
        if (!model) throw new NotFoundException();

        /**
         * Verificar autorização
         */
        if (!this.verifyAuthorization('get', { model: model as T, data: _id })) {
            throw new UnauthorizedException();
        }

        return model;
    }

    /**
     * Atualizar model
     * @param id
     * @param data
     */
    async update<B>(_id: string, data: B): Promise<boolean> {
        const model = await this.__get(_id);

        /**
         * Verificar autorização
         */
        if (
            !this.verifyAuthorization('update', {
                model: model as T,
                data: _id,
            })
        ) {
            throw new UnauthorizedException();
        }

        /**
         * Tratamento de dados
         */
        data = await this.beforeDataHandling('update', data, {});

        return this.__update((model as any).id, data);
    }

    /**
     * Delete model
     * @param id
     */
    async delete(_id: string): Promise<boolean> {
        const model = await this.__get(_id);

        /**
         * Verificar autorização
         */
        if (
            !this.verifyAuthorization('delete', {
                model: model as T,
                data: _id,
            })
        ) {
            throw new UnauthorizedException();
        }

        let data = {
            enabled: 0,
            deleted_at: format(new Date(), 'yyyy-MM-dd hh:mm:ss'),
        };

        /**
         * Tratamento do modelo
         */
        data = await this.beforeDataHandling('search', data, {
            data,
            model,
        });

        return this.__update((model as any).id, data);
    }

    /**
     * Buscar model
     * @param data
     */
    async search<B, B2>(data: IBodySearch<B>): Promise<IResultSearch<B2>> {
        /**
         * Verificar autorização
         */
        if (!this.verifyAuthorization('search', {})) {
            throw new UnauthorizedException();
        }

        /**
         * Tratamento de dados
         */
        data = await this.beforeDataHandling('search', data, {});

        /**
         * Criar search
         */
        const queryBuilder = this.__search(data.filters, data.query, data.alias);

        /**
         * Tratamento de Query Builder
         */
        await this.searchHandlingQueryBuilder(queryBuilder, {
            data,
        });

        const results: IResultSearch<B2> = {
            count: 0,
            results: [],
        };

        if (data && data.query && data.query.limit == 0) {
            results.results = [];
        } else {
            results.results = await queryBuilder;
        }

        results.count = await queryBuilder.countDocuments();

        return results;
    }

    /**
     * Obter modelo
     * @param body
     */
    async __get(_id: string): Promise<T> {
        const model = await this.model.findOne({
            _id,
            enabled: 1,
        });

        return Promise.resolve(model as T);
    }

    /**
     * Atualizar Modelo com dados enviados
     * @param id
     * @param body
     * @private
     */
    async __update(_id: string, body: any): Promise<boolean> {
        const modelUpdate = await this.model.updateOne(
            {
                _id,
                enabled: 1,
            },
            body,
        );

        return !!modelUpdate.ok;
    }

    /**
     * Lógica para filtrar model da DB
     * @param filters
     * @param query
     * @param alias
     */
    protected __search(filters: any, query: any, alias: string[]): DocumentQuery<any, any> {
        let queryBuilder = this.model.find();

        if (!query) query = {};
        if (!filters) filters = {};

        queryBuilder = this.__filters(queryBuilder, filters || {});
        // queryBuilder = this.__query(queryBuilder, query || {});
        // queryBuilder = this.__sorts(queryBuilder, query.sorts || []);
        // queryBuilder = this.__projection(queryBuilder, alias || []);

        return queryBuilder;
    }

    /**
     * Remove aliass ModelsName__ dos resultados
     * @param documentQuery
     * @private
     */
    // private async __getRawMany(documentQuery: DocumentQuery<any, any>) {
    //     let results = await documentQuery.getRawMany();
    //     let newResults = [];

    //     for (let row of results) {
    //         let newRow = {};
    //         for (let i in row) {
    //             newRow[i.replace(this.model.name + '_', '')] = row[i];
    //         }
    //         newResults.push(newRow);
    //     }

    //     return newResults;
    // }

    /**
     * Define limit, offset
     * @param resource
     * @param query
     */
    public __query(resource: DocumentQuery<any, any>, query: IQuery): DocumentQuery<any, any> {
        let limit = this.limitDefault;
        if (query.limit && !Number.isNaN(Number(query.limit)) && query.limit > 0) {
            resource.limit(Math.min(this.limitMax, Number(query.limit)));
            limit = Math.min(this.limitMax, Number(query.limit));
        } else {
            resource.limit(this.limitDefault);
        }

        if (query.offset && !Number.isNaN(Number(query.offset)) && query.offset > 0) {
            resource.skip(Number(query.offset));
        }

        if (query.page && !Number.isNaN(Number(query.page)) && query.page > 0) {
            resource.skip((Number(query.page) - 1) * limit);
        }

        return resource;
    }

    /**
     * Define quais valores devem ser retornados na busca do banco de dados
     * @param resource
     * @param projections
     */
    public __projection(resource: DocumentQuery<any, any>, projections: string[]): DocumentQuery<any, any> {
        const newProj = {};

        /**
         * Adicionar projections enviados pela requisição
         * Caso não seja enviado nenhum valor de projections
         * DB deve retornar todos os valores
         */
        if (projections.length > 0) {
            projections.forEach(element => {
                if (typeof element === 'string') {
                    newProj[element] = 1;
                }
            });

            /**
             * Executar merge em cima dos projections enviados pela requisição
             */
            if (this.aliases.length > 0) {
                this.aliases.forEach(element => {
                    if (typeof element === 'string') {
                        newProj[element] = 1;
                    }
                });
            }

            /**
             * Executa select
             */
            resource.select(newProj);
        }

        return resource;
    }

    /**
     * Define ordem dos resultados
     * Verifica se valores enviados estão disponiveis para uso
     * @param resource
     * @param sortVal
     */
    public __sorts(resource: DocumentQuery<any, any>, sortVal: string[]): DocumentQuery<any, any> {
        // Preparar o resource com o sort
        const data = {
            sort: {},
        };
        /**
         * Verificar se os itens enviados para o servidor estão na lista de sort
         */
        for (let i = 0; i < this.sort.length; i++) {
            const mainSort = this.sort[i];

            for (let iS = 0; iS < sortVal.length; iS++) {
                const reqSort = sortVal[iS];
                let checkSort = sortVal[iS].toString();

                /**
                 * Se valor for uma script apenas compara
                 */
                if (typeof mainSort === 'string') {
                    if (reqSort.indexOf('-') === 0) {
                        checkSort = reqSort.substring(1);
                    }

                    if (checkSort === mainSort) {
                        if (reqSort.indexOf('-') === 0) {
                            data.sort[checkSort as string] = -1;
                        } else {
                            data.sort[checkSort as string] = 1;
                        }
                    }

                    /**
                     * Se o elemento de sort não for uma string
                     * verificar valores que existem dentro do objeto
                     */
                } else if (typeof mainSort === 'object') {
                    if (reqSort.indexOf('-') === 0) {
                        checkSort = reqSort.substring(1);
                    }

                    if (mainSort.name === checkSort) {
                        if (reqSort.indexOf('-') === 0) {
                            data.sort[mainSort.alias] = -1;
                        } else {
                            data.sort[mainSort.alias] = 1;
                        }
                    }
                }
            }
        }

        // Caso nao tenha nada para ser sorteado sera enviado o sort default
        if (this.sort.length === 0 || (Object.entries(data.sort).length === 0 && data.sort.constructor === Object)) {
            data.sort = this.sortDefault;
        }

        return resource.find().setOptions(data);
    }

    // public __count(resource: DocumentQuery<any, any>): Promise<number> {
    // 	return await resource.count({});
    // }

    /**
     * Define os filtros que serão usados na pesquisa
     * no Model Mongoose enviado
     * @param resource
     * @param filters
     */
    public __filters(resource: DocumentQuery<any, any>, filters: { [key: string]: any }): DocumentQuery<any, any> {
        /**
         * Filters Allowed
         */
        for (const key in this.filters) {
            /**
             * Filters process
             */
            if (this.filters.hasOwnProperty(key)) {
                const filterConfig = this.filters[key];

                for (const key2 in filters) {
                    if (filters.hasOwnProperty(key2)) {
                        const filterItem = filters[key2];

                        /**
                         * Compara filters com filtersallowed
                         */
                        if (key === key2) {
                            switch (filterConfig.filter) {
                                case 'int':
                                    resource.where(filterConfig.column, Number(filterItem));
                                    break;

                                case 'intGt':
                                    resource.gt(filterConfig.column, Number(filterItem));
                                    break;

                                case 'intLt':
                                    resource.lt(filterConfig.column, Number(filterItem));
                                    break;

                                case 'intGte':
                                    resource.gte(filterConfig.column, Number(filterItem));

                                    break;

                                case 'intLte':
                                    resource.lte(filterConfig.column, Number(filterItem));
                                    break;

                                case 'string':
                                    resource.where(filterConfig.column, filterItem);
                                    break;

                                case 'stringLike':
                                    resource.where(filterConfig.column, {
                                        $regex: `^${filterItem}$`,
                                        $options: 'i',
                                    });
                                    break;

                                case 'stringLikeNot':
                                    resource.where(filterConfig.column, {
                                        $regex: `^((?!${filterItem}).)*$`,
                                        $options: 'i',
                                    });
                                    break;

                                case 'arrayInt':
                                    if (Helpers.typeOf(filterItem) === 'array') {
                                        resource.in(filterConfig.column, filterItem.map(Number));
                                    }
                                    break;

                                case 'arrayIntNot':
                                    if (Helpers.typeOf(filterItem) === 'array') {
                                        resource.nin(filterConfig.column, filterItem.map(Number));
                                    }
                                    break;

                                case 'arrayString':
                                    if (Helpers.typeOf(filterItem) === 'array') {
                                        resource.in(filterConfig.column, filterItem);
                                    }
                                    break;

                                case 'arrayStringNot':
                                    if (Helpers.typeOf(filterItem) === 'array') {
                                        resource.nin(filterConfig.column, filterItem);
                                    }
                                    break;

                                case 'stringLikePercent':
                                    resource.where(filterConfig.column, {
                                        $regex: filterItem,
                                        $options: 'i',
                                    });
                                    break;

                                case 'stringLikePercentAfter':
                                    resource.where(filterConfig.column, {
                                        $regex: `${filterItem}$`,
                                        $options: 'i',
                                    });
                                    break;

                                case 'stringLikePercentBefore':
                                    resource.where(filterConfig.column, {
                                        $regex: `^${filterItem}`,
                                        $options: 'i',
                                    });
                                    break;
                            }
                        }
                    }
                }
            }
        }

        return resource;
    }
}
